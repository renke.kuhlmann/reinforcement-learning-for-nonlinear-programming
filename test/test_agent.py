#!/usr/bin/env python3
# pylint: disable=missing-docstring,protected-access

import unittest
import numpy as np

from app.agent import Agent
from app.task import Task
from app.stats import StatsAgent

class TestAgent(unittest.TestCase):

    def test_agent_action(self):
        class TaskTest(Task):
            def get_actions(self, state):
                return [0, 1, 2, 3, 4, 5]

        par = {}
        task = TaskTest(par)
        agent = Agent(task, par)
        miniagent = agent.get_miniagent(0)

        self.assertEqual(agent.pretrain_envs(['a', 'b', 'c']), [])

        for _ in range(10):
            task.default_action = np.random.rand(1)
            state = np.random.rand(5)
            self.assertEqual(agent.action(state), task.default_action)
            self.assertEqual(miniagent.action(state), task.default_action)

        for _ in range(10):
            state = np.random.rand(5)
            self.assertTrue(0 <= agent.action_rand(state) <= 5)
            self.assertTrue(0 <= miniagent.action_rand(state) <= 5)


    def test_agent_train(self):
        par = {}
        task = Task(par)
        agent = Agent(task, par)

        self.assertIsInstance(agent.train_episode([]), StatsAgent)
        self.assertIsInstance(agent.train_epoch([]), StatsAgent)


    def test_agent_update_regret(self):
        par = {}
        task = Task(par)
        agent = Agent(task, par)

        ref_rewards = {'a': -100, 'b': -40}
        agent.update_regret_ref(ref_rewards)
        self.assertEqual(agent.ref_rewards['a'], -100)
        self.assertEqual(agent.ref_rewards['b'], -40)

        ref_rewards = {'a': -40, 'b': -100}
        agent.update_regret_ref(ref_rewards)
        self.assertEqual(agent.ref_rewards['a'], -40)
        self.assertEqual(agent.ref_rewards['b'], -40)

        ref_rewards = {'a': -200, 'b': -20}
        agent.update_regret_ref(ref_rewards)
        self.assertEqual(agent.ref_rewards['a'], -40)
        self.assertEqual(agent.ref_rewards['b'], -20)

        ref_rewards = {'a': 100, 'b': 0}
        agent.update_regret_ref(ref_rewards)
        self.assertEqual(agent.ref_rewards['a'], 100)
        self.assertEqual(agent.ref_rewards['b'], 0)


if __name__ == '__main__':
    unittest.main()
