#!/usr/bin/env python3
# pylint: disable=missing-docstring,protected-access,no-member

import os
import unittest
import numpy as np
import tensorflow as tf

from app.agent_q import AgentQ
from app.task import Task

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

class TestAgentQ(unittest.TestCase):

    def test_agent_q_neural_network_forward(self):
        par = {'threads': {'num': 0,
                           'host': '127.0.0.1',
                           'ports2': 5150},
               'agent_q': {'nn_dueling_q': False,
                           'nn_hidden_nodes': 50,
                           'memory_size': 1000,
                           'target_update_frac': 1.0},
               'training': {'state_recurrence': 0}}
        task = Task(par)
        task.dim_states = 10
        task.dim_actions = 5
        agent = AgentQ(task, par)

        for _ in range(10):
            state = np.random.rand(10)
            self.assertTrue(0 <= agent.action(state) <= task.dim_actions-1)

        agent.finalize_tensorflow()


    def test_agent_q_target_update(self):
        par = {'threads': {'num': 1,
                           'host': '127.0.0.1',
                           'ports2': 5160},
               'agent_q': {'nn_dueling_q': False,
                           'nn_hidden_nodes': 50,
                           'memory_size': 1000,
                           'target_update_frac': 1.0},
               'training': {'state_recurrence': 1}}
        task = Task(par)
        task.dim_states = 10
        task.dim_actions = 5
        agent = AgentQ(task, par)

        main_weights = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, 'current')
        target_weights = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, 'target')
        agent.sess.run(tf.global_variables_initializer())
        update_op = agent._neural_network_update_target(1.0)
        self.assertTrue(len(update_op) > 0)

        agent.sess.run(update_op)
        main_weights_val = agent.sess.run(main_weights)
        target_weights_val = agent.sess.run(target_weights)

        for main_val, target_val in zip(main_weights_val, target_weights_val):
            for main_val_1, target_val_1 in zip(main_val, target_val):
                self.assertTrue((main_val_1 == target_val_1).all())

        agent.finalize_tensorflow()


    def test_agent_q_experience_replay(self):
        par = {'threads': {'num': 0,
                           'host': '127.0.0.1',
                           'ports2': 5170},
               'agent_q': {'nn_dueling_q': False,
                           'nn_hidden_nodes': 50,
                           'memory_size': 1000,
                           'target_update_frac': 1.0,
                           'regret': False},
               'training': {'state_recurrence': 1}}
        task = Task(par)
        task.dim_states = 3
        task.dim_actions = 5
        agent = AgentQ(task, par)

        observations = []
        for i in range(20):
            observations.append({'name': 'test', 'terminal': False,
                                 'state': [[i, i, i]], 'action': 100 + i,
                                 'reward': -i, 'next_state': [[i+1, i+1, i+1]]})
        observations.append({'name': 'test', 'terminal': True,
                             'state': [20, 20, 20]})

        for obs in observations:
            agent.pretrain_episode(obs)

        for i, experience in enumerate(agent.memory.buffer_episodes[0]):
            self.assertEqual(experience['name'], 'test')
            if i < 20:
                self.assertFalse(experience['terminal'])
                self.assertTrue((experience['state'] == [i, i, i]).all())
                self.assertEqual(experience['action'], 100+i)
                self.assertEqual(experience['reward'], -i)
                self.assertTrue((experience['next_state'] == [i+1, i+1, i+1]).all())
            else:
                self.assertTrue(experience['terminal'])
                self.assertTrue((experience['state'] == [20, 20, 20]).all())

        agent.finalize_tensorflow()

        par['threads']['ports2'] = 5180
        agent = AgentQ(task, par)
        agent.pretrain_epoch([('test', observations)])

        for i, experience in enumerate(agent.memory.buffer_episodes[0]):
            self.assertEqual(experience['name'], 'test')
            if i < 20:
                self.assertFalse(experience['terminal'])
                self.assertTrue((experience['state'] == [i, i, i]).all())
                self.assertEqual(experience['action'], 100+i)
                self.assertEqual(experience['reward'], -i)
                self.assertTrue((experience['next_state'] == [i+1, i+1, i+1]).all())
            else:
                self.assertTrue(experience['terminal'])
                self.assertTrue((experience['state'] == [20, 20, 20]).all())

        agent.finalize_tensorflow()


    def test_agent_q_experience_replay_delayed(self):
        par = {'threads': {'num': 0,
                           'host': '127.0.0.1',
                           'ports2': 5190},
               'agent_q': {'nn_dueling_q': False,
                           'nn_hidden_nodes': 50,
                           'memory_size': 1000,
                           'target_update_frac': 1.0,
                           'regret': True},
               'training': {'state_recurrence': 1}}
        task = Task(par)
        task.dim_states = 3
        task.dim_actions = 5
        agent = AgentQ(task, par)

        observations = []
        for i in range(20):
            observations.append({'name': 'test', 'terminal': False,
                                 'state': [[i, i, i]], 'action': 100 + i,
                                 'reward': -i, 'next_state': [[i+1, i+1, i+1]]})
        observation_final = {'name': 'test', 'terminal': True,
                             'state': [20, 20, 20]}

        for obs in observations:
            agent.pretrain_episode(obs)

        self.assertEqual(agent.memory.size1('test'), 0)
        self.assertEqual(agent.memory_episode.size1('test'), 20)

        agent.pretrain_episode(observation_final)

        self.assertEqual(agent.memory.size1('test'), 21)
        self.assertEqual(agent.memory_episode.size1('test'), 0)

        for i, experience in enumerate(agent.memory.buffer_episodes[0]):
            self.assertEqual(experience['name'], 'test')
            self.assertEqual(experience['num_steps'], 20)
            if i < 20:
                self.assertFalse(experience['terminal'])
                self.assertTrue((experience['state'] == [i, i, i]).all())
                self.assertEqual(experience['action'], 100+i)
                self.assertEqual(experience['reward'], -i)
                self.assertTrue((experience['next_state'] == [i+1, i+1, i+1]).all())
            else:
                self.assertTrue(experience['terminal'])
                self.assertTrue((experience['state'] == [20, 20, 20]).all())

        agent.finalize_tensorflow()

        par['threads']['ports2'] = 5200
        agent = AgentQ(task, par)

        observations.append(observation_final)
        agent.pretrain_epoch([('test', observations)])

        for i, experience in enumerate(agent.memory.buffer_episodes[0]):
            self.assertEqual(experience['name'], 'test')
            self.assertEqual(experience['num_steps'], 20)
            if i < 20:
                self.assertFalse(experience['terminal'])
                self.assertTrue((experience['state'] == [i, i, i]).all())
                self.assertEqual(experience['action'], 100+i)
                self.assertEqual(experience['reward'], -i)
                self.assertTrue((experience['next_state'] == [i+1, i+1, i+1]).all())
            else:
                self.assertTrue(experience['terminal'])
                self.assertTrue((experience['state'] == [20, 20, 20]).all())

        agent.finalize_tensorflow()

if __name__ == '__main__':
    unittest.main()
