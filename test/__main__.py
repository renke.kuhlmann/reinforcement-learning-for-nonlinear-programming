#!/usr/bin/env python3
# pylint: disable=missing-docstring

import os
import sys
import unittest

TESTDIR = os.path.dirname(__file__)
sys.path.insert(0, os.path.abspath(os.path.join(TESTDIR, '..')))

def run_tests():
    loader = unittest.TestLoader()
    test_suite = loader.discover('test', pattern='test*.py')
    unittest.TextTestRunner(verbosity=2).run(test_suite)

def run_integration_tests():
    loader = unittest.TestLoader()
    test_suite = loader.discover('test', pattern='inttest*.py')
    unittest.TextTestRunner(verbosity=2).run(test_suite)

if __name__ == '__main__':
    ENABLE_TESTS = '--tests' in sys.argv
    ENABLE_INTTESTS = '--inttests' in sys.argv
    if not ENABLE_TESTS and not ENABLE_INTTESTS:
        ENABLE_TESTS = True
        ENABLE_INTTESTS = True

    if ENABLE_TESTS:
        print("")
        print("-"*70)
        print("UNIT TESTS")
        print("-"*70)
        run_tests()
    if ENABLE_INTTESTS:
        print("")
        print("-"*70)
        print("INTEGRATION TESTS")
        print("-"*70)
        run_integration_tests()
