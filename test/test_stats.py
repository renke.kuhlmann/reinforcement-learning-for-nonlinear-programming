#!/usr/bin/env python3
# pylint: disable=missing-docstring

import unittest
import time
from app.stats import Stats, StatsLearn

class TestStats(unittest.TestCase):

    def test_stats_timer(self):
        stats = Stats()
        time.sleep(0.01)
        stats.time_stop()
        time_tmp = stats.time
        self.assertTrue(time_tmp >= 0.01)
        self.assertTrue(time_tmp <= 0.1)
        time.sleep(0.01)
        self.assertEqual(time_tmp, stats.time)


class TestStatsLearn(unittest.TestCase):

    def test_stats_timer(self):
        stats = StatsLearn(['a', 'b', 'c'])
        time.sleep(0.01)
        stats.time_stop()
        time_tmp = stats.time
        self.assertTrue(time_tmp >= 0.01)
        self.assertTrue(time_tmp <= 0.1)
        time.sleep(0.01)
        self.assertEqual(time_tmp, stats.time)


    def test_stats_reset(self):
        stats = StatsLearn(['a', 'b', 'c'])
        stats.rewards['a'] = 10
        stats.rewards['b'] = 20
        stats.rewards_best['a'] = 20
        stats.rewards_best['b'] = 30
        stats.performances.append(1)
        stats.performances_best = 1
        stats.reward_reset()
        stats.performance_reset()
        for _, reward in stats.rewards.items():
            self.assertEqual(reward, -float("inf"))
        for _, reward in stats.rewards_best.items():
            self.assertEqual(reward, -float("inf"))
        self.assertEqual(stats.performances, [])
        self.assertEqual(stats.performance_best, -float("inf"))


    def test_stats_update_epoch(self):
        stats = StatsLearn(['a', 'b', 'c'])
        stats.update_epoch(10, {'a': -4, 'b': -6, 'c': -10}, -3.33)
        self.assertEqual(stats.epochs[-1], 10)
        self.assertEqual(stats.rewards_best['a'], -4)
        self.assertEqual(stats.rewards_best['b'], -6)
        self.assertEqual(stats.rewards_best['c'], -10)
        self.assertEqual(stats.performances[-1], -3.33)
        self.assertEqual(stats.performance_best, -3.33)
        stats.update_epoch(20, {'a': -1, 'b': -16, 'c': -10}, -4)
        self.assertEqual(stats.epochs[-1], 20)
        self.assertEqual(stats.rewards_best['a'], -1)
        self.assertEqual(stats.rewards_best['b'], -6)
        self.assertEqual(stats.rewards_best['c'], -10)
        self.assertEqual(stats.performances[-1], -4)
        self.assertEqual(stats.performance_best, -3.33)
        stats.update_epoch(30, {'a': -10, 'b': 0, 'c': 1}, -1)
        self.assertEqual(stats.epochs[-1], 30)
        self.assertEqual(stats.rewards_best['a'], -1)
        self.assertEqual(stats.rewards_best['b'], 0)
        self.assertEqual(stats.rewards_best['c'], 1)
        self.assertEqual(stats.performances[-1], -1)
        self.assertEqual(stats.performance_best, -1)

if __name__ == '__main__':
    unittest.main()
