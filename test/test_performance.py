#!/usr/bin/env python3
# pylint: disable=missing-docstring

import unittest
import numpy as np
from app.performance import Performance

class TestPerformance(unittest.TestCase):

    def test_performance_geomean(self):
        self.assertAlmostEqual(Performance.geo_mean(np.array([0, 0]), 1), 0)
        self.assertAlmostEqual(Performance.geo_mean(np.array([1, 1]), 1), 1)
        self.assertAlmostEqual(Performance.geo_mean(np.array([2, 2]), 1), 2)

        self.assertAlmostEqual(Performance.geo_mean(np.array([1, 1]), 1), 1)
        self.assertAlmostEqual(Performance.geo_mean(np.array([1, 1]), 2), 1)
        self.assertAlmostEqual(Performance.geo_mean(np.array([1, 1]), 3), 1)

    def test_performance_profile(self):
        mat = np.array([[1, 1, 1],
                        [2, 2, 2]])
        _, rhos = Performance.performance_profile(mat, 1000)
        for rho in rhos:
            self.assertAlmostEqual(rho[0], 100)
            self.assertTrue(round(rho[1] - 100) == 0 or round(rho[1]) == 0)

        mat = np.array([[1, 1, 1],
                        [1001, 1001, 1001]])
        _, rhos = Performance.performance_profile(mat, 1000)
        for rho in rhos:
            self.assertAlmostEqual(rho[0], 100)
            self.assertAlmostEqual(rho[1], 0)

        for _ in range(10):
            _, rhos = Performance.performance_profile(np.random.rand(4, 10)*1500, 1000)
            self.assertEqual(rhos.shape[1], 4)
            for rho in rhos:
                self.assertTrue(0 <= np.max(rho) <= 100)
                self.assertTrue(0 <= np.min(rho) <= 100)

    def test_performance_smooth(self):
        x_val = np.arange(1000)
        y_val = np.zeros((1000,))
        _, y_smooth = Performance.smooth(x_val, y_val, 10)
        for _, y_smooth_i in enumerate(y_smooth):
            self.assertAlmostEqual(y_smooth_i, 0)

        for _ in range(10):
            x_val = np.arange(100)
            y_val = np.random.rand(100)
            _, y_smooth = Performance.smooth(x_val, y_val, 10)
            for _, y_smooth_i in enumerate(y_smooth):
                self.assertTrue(np.min(y_val) <= y_smooth_i <= np.max(y_val))

    def test_performance_calc_measure_geomean(self):
        par = {'performance': {'baseline': 'worhp',
                               'method': 'geomean',
                               'geomean_shift': 10},
               'nlp': {'maxiter': 1000}}
        perf = Performance(par)

        rewards = {'MPC1': -900, 'MPC2': -200, 'MPC3': -250}
        perf_val = perf.performance(rewards)
        self.assertTrue(perf_val < 0)

        rewards = {'MPC1': -1, 'MPC2': -1, 'MPC3': -1}
        perf_val = perf.performance(rewards)
        self.assertTrue(perf_val > 0)

        rewards = {'MPC1': perf.baseline_rewards[perf.baseline_idx['MPC1']],
                   'MPC2': perf.baseline_rewards[perf.baseline_idx['MPC2']],
                   'MPC3': perf.baseline_rewards[perf.baseline_idx['MPC3']]}
        perf_val = perf.performance(rewards)
        self.assertAlmostEqual(perf_val, 0)

    def test_performance_calc_measure_profile(self):
        par = {'performance': {'baseline': 'worhp',
                               'method': 'pprofiles',
                               'geomean_shift': 10},
               'nlp': {'maxiter': 1000}}
        perf = Performance(par)

        rewards = {'MPC1': -900, 'MPC2': -200, 'MPC3': -250}
        perf_val = perf.performance(rewards)
        self.assertTrue(-1 <= perf_val < 0)

        rewards = {'MPC1': -1, 'MPC2': -1, 'MPC3': -1}
        perf_val = perf.performance(rewards)
        self.assertTrue(0 < perf_val <= 1)

        rewards = {'MPC1': perf.baseline_rewards[perf.baseline_idx['MPC1']],
                   'MPC2': perf.baseline_rewards[perf.baseline_idx['MPC2']],
                   'MPC3': perf.baseline_rewards[perf.baseline_idx['MPC3']]}
        perf_val = perf.performance(rewards)
        self.assertAlmostEqual(perf_val, 0)

    def test_performance_improve(self):
        self.assertTrue(Performance.performance_improve(1.0, 0.0))
        self.assertFalse(Performance.performance_improve(1.0, 1.0))
        self.assertFalse(Performance.performance_improve(1.0, 2.0))


if __name__ == '__main__':
    unittest.main()
