#!/usr/bin/env python3
# pylint: disable=missing-docstring

import sys
import os
import stat
import unittest
import time
import threading
from io import StringIO

from app.markov import MarkovEnvCommunicator, MarkovAgentThread, MarkovLoop
from app.markov import MarkovAgentCommunicator, Markov
from app.agent import Agent, MiniAgent
from app.stats import StatsLearn

def test_markov(par):
    # pylint: disable=too-many-locals

    # create python environment that keeps sending states and rewards and
    # wants to get some action
    prog1 = """
#!/usr/bin/env python3
import sys
import socket
host = sys.argv[3]
port = int(sys.argv[5])
print(host, port)
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((host, port))
    for i in range(100):
        msg = "<<STATE:{0:d}:{1:d}:{2:d}|REWARD:{3:d}|ACTION?>>".format(i, i, i, -i)
        s.send(msg.encode())
        s.recv(1024).decode()
    msg = "<<STATE:{0:d}:{1:d}:{2:d}>>".format(100, 100, 100)
    s.send(msg.encode())
    s.send("<<DONE>>".encode())
    """
    prog2 = """
#!/usr/bin/env python3
import sys
import socket
host = sys.argv[3]
port = int(sys.argv[5])
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((host, port))
    for i in range(100):
        msg = "<<STATE:{0:d}:{1:d}:{2:d}|REWARD:{3:d}|ACTION?>>".format(-i, -i, -i, i)
        s.send(msg.encode())
        s.recv(1024).decode()
    msg = "<<STATE:{0:d}:{1:d}:{2:d}>>".format(-100, -100, -100)
    s.send(msg.encode())
    s.send("<<DONE>>".encode())
    """
    directory = os.path.abspath('build/worhp_ai')
    if not os.path.exists(directory):
        os.makedirs(directory)
    filename = os.path.join(directory, 'test_simple_env1.py')
    with open(filename, "w") as file:
        file.write(prog1)
    filename = os.path.join(directory, 'test_simple_env2.py')
    with open(filename, "w") as file:
        file.write(prog2)
    filename = os.path.join(directory, 'test_simple_env_run1')
    with open(filename, "w") as file:
        file.write("#!/bin/bash \npython " + directory + "/test_simple_env1.py $1 $2 $3 $4 $5")
    file_stat = os.stat(filename)
    os.chmod(filename, file_stat.st_mode | stat.S_IEXEC)
    filename = os.path.join(directory, 'test_simple_env_run2')
    with open(filename, "w") as file:
        file.write("#!/bin/bash \npython " + directory + "/test_simple_env2.py $1 $2 $3 $4 $5")
    file_stat = os.stat(filename)
    os.chmod(filename, file_stat.st_mode | stat.S_IEXEC)

    class AgentTest(Agent):
        def action(self, state):
            return 42

        def action_rand(self, state):
            return 43

        def train_episode(self, _):
            return 44

        def pretrain_episode(self, _):
            return 45

        def get_miniagent(self, thread_id):
            return MiniAgentTest(self.task)

    class MiniAgentTest(MiniAgent):
        def action(self, state):
            return 42

        def action_rand(self, state):
            return 43


    # redirect stdout
    old_stdout = sys.stdout
    result = StringIO()
    sys.stdout = result

    # start markov
    agent = AgentTest(None, None)
    markov = Markov(agent, par)
    env_list = ['test_simple_env_run1', 'test_simple_env_run2']
    stats_learn = StatsLearn(env_list)
    observations, _, _ = markov.run(env_list, 'training', 0, stats_learn)
    markov.finalize()

    # normal stdout
    sys.stdout = old_stdout

    return observations


class TestMarkov(unittest.TestCase):

    def test_markov_episode_copy_1(self):
        par = {'threads': {'num': 1,
                           'host': '127.0.0.1',
                           'ports1': 5000},
               'training': {'explore_start': 0,
                            'explore_stop': 0,
                            'explore_decay_rate': 0,
                            'state_recurrence': 0},
               'learning': {'train_call': 'episode',
                            'action_call': 'copy'},
               'nlp': {'maxiter': 1000}}

        observations = test_markov(par)

        self.assertEqual(len(observations), 2)
        for obs_env in observations:
            self.assertEqual(len(obs_env), 2)
            self.assertTrue(obs_env[0] == 'test_simple_env_run1' or
                            obs_env[0] == 'test_simple_env_run2')
            self.assertEqual(len(obs_env[1]), 101)
            obs = obs_env[1]
            sign = 0
            if obs_env[0] == 'test_simple_env_run1':
                sign = 1
            elif obs_env[0] == 'test_simple_env_run2':
                sign = -1
            for i in range(100):
                self.assertTrue('state' in obs[i])
                self.assertTrue('action' in obs[i])
                self.assertTrue('reward' in obs[i])
                self.assertTrue('next_state' in obs[i])
                self.assertTrue('terminal' in obs[i])
                self.assertEqual(obs[i]['state'], [[sign*i]*3])
                self.assertEqual(obs[i]['action'], 42)
                self.assertEqual(obs[i]['next_state'], [[sign*(i+1)]*3])
                self.assertEqual(obs[i]['reward'], -sign * i)
                self.assertFalse(obs[i]['terminal'])
            self.assertTrue('state' in obs[100])
            self.assertFalse('action' in obs[100])
            self.assertFalse('reward' in obs[100])
            self.assertFalse('next_state' in obs[100])
            self.assertTrue('terminal' in obs[100])
            self.assertEqual(obs[100]['state'], [[sign*100]*3])
            self.assertTrue(obs[100]['terminal'])

    def test_markov_episode_copy_2(self):
        par = {'threads': {'num': 2,
                           'host': '127.0.0.1',
                           'ports1': 5000},
               'training': {'explore_start': 0,
                            'explore_stop': 0,
                            'explore_decay_rate': 0,
                            'state_recurrence': 0},
               'learning': {'train_call': 'episode',
                            'action_call': 'copy'},
               'nlp': {'maxiter': 1000}}

        observations = test_markov(par)

        self.assertEqual(len(observations), 2)
        for obs_env in observations:
            self.assertEqual(len(obs_env), 2)
            self.assertTrue(obs_env[0] == 'test_simple_env_run1' or
                            obs_env[0] == 'test_simple_env_run2')
            self.assertEqual(len(obs_env[1]), 101)
            obs = obs_env[1]
            sign = 0
            if obs_env[0] == 'test_simple_env_run1':
                sign = 1
            elif obs_env[0] == 'test_simple_env_run2':
                sign = -1
            for i in range(100):
                self.assertTrue('state' in obs[i])
                self.assertTrue('action' in obs[i])
                self.assertTrue('reward' in obs[i])
                self.assertTrue('next_state' in obs[i])
                self.assertTrue('terminal' in obs[i])
                self.assertEqual(obs[i]['state'], [[sign*i]*3])
                self.assertEqual(obs[i]['action'], 42)
                self.assertEqual(obs[i]['next_state'], [[sign*(i+1)]*3])
                self.assertEqual(obs[i]['reward'], -sign * i)
                self.assertFalse(obs[i]['terminal'])
            self.assertTrue('state' in obs[100])
            self.assertFalse('action' in obs[100])
            self.assertFalse('reward' in obs[100])
            self.assertFalse('next_state' in obs[100])
            self.assertTrue('terminal' in obs[100])
            self.assertEqual(obs[100]['state'], [[sign*100]*3])
            self.assertTrue(obs[100]['terminal'])

    def test_markov_episode_thread(self):
        par = {'threads': {'num': 2,
                           'host': '127.0.0.1',
                           'ports1': 5000},
               'training': {'explore_start': 0,
                            'explore_stop': 0,
                            'explore_decay_rate': 0,
                            'state_recurrence': 0},
               'learning': {'train_call': 'episode',
                            'action_call': 'thread'},
               'nlp': {'maxiter': 1000}}

        observations = test_markov(par)

        self.assertEqual(len(observations), 2)
        for obs_env in observations:
            self.assertEqual(len(obs_env), 2)
            self.assertTrue(obs_env[0] == 'test_simple_env_run1' or
                            obs_env[0] == 'test_simple_env_run2')
            self.assertEqual(len(obs_env[1]), 101)
            obs = obs_env[1]
            sign = 0
            if obs_env[0] == 'test_simple_env_run1':
                sign = 1
            elif obs_env[0] == 'test_simple_env_run2':
                sign = -1
            for i in range(100):
                self.assertTrue('state' in obs[i])
                self.assertTrue('action' in obs[i])
                self.assertTrue('reward' in obs[i])
                self.assertTrue('next_state' in obs[i])
                self.assertTrue('terminal' in obs[i])
                self.assertEqual(obs[i]['state'], [[sign*i]*3])
                self.assertEqual(obs[i]['action'], 42)
                self.assertEqual(obs[i]['next_state'], [[sign*(i+1)]*3])
                self.assertEqual(obs[i]['reward'], -sign * i)
                self.assertFalse(obs[i]['terminal'])
            self.assertTrue('state' in obs[100])
            self.assertFalse('action' in obs[100])
            self.assertFalse('reward' in obs[100])
            self.assertFalse('next_state' in obs[100])
            self.assertTrue('terminal' in obs[100])
            self.assertEqual(obs[100]['state'], [[sign*100]*3])
            self.assertTrue(obs[100]['terminal'])

    def test_markov_epoch_copy(self):
        par = {'threads': {'num': 2,
                           'host': '127.0.0.1',
                           'ports1': 5000},
               'training': {'explore_start': 0,
                            'explore_stop': 0,
                            'explore_decay_rate': 0,
                            'state_recurrence': 0},
               'learning': {'train_call': 'epoch',
                            'action_call': 'copy'},
               'nlp': {'maxiter': 1000}}

        observations = test_markov(par)

        self.assertEqual(len(observations), 2)
        for obs_env in observations:
            self.assertEqual(len(obs_env), 2)
            self.assertTrue(obs_env[0] == 'test_simple_env_run1' or
                            obs_env[0] == 'test_simple_env_run2')
            self.assertEqual(len(obs_env[1]), 101)
            obs = obs_env[1]
            sign = 0
            if obs_env[0] == 'test_simple_env_run1':
                sign = 1
            elif obs_env[0] == 'test_simple_env_run2':
                sign = -1
            for i in range(100):
                self.assertTrue('state' in obs[i])
                self.assertTrue('action' in obs[i])
                self.assertTrue('reward' in obs[i])
                self.assertTrue('next_state' in obs[i])
                self.assertTrue('terminal' in obs[i])
                self.assertEqual(obs[i]['state'], [[sign*i]*3])
                self.assertEqual(obs[i]['action'], 42)
                self.assertEqual(obs[i]['next_state'], [[sign*(i+1)]*3])
                self.assertEqual(obs[i]['reward'], -sign * i)
                self.assertFalse(obs[i]['terminal'])
            self.assertTrue('state' in obs[100])
            self.assertFalse('action' in obs[100])
            self.assertFalse('reward' in obs[100])
            self.assertFalse('next_state' in obs[100])
            self.assertTrue('terminal' in obs[100])
            self.assertEqual(obs[100]['state'], [[sign*100]*3])
            self.assertTrue(obs[100]['terminal'])


class TestMarkovEnvCommunicator(unittest.TestCase):

    def test_markov_socket_communication(self):

        # create python communicator, that responds what it receives
        prog = """
#!/usr/bin/env python3
import socket
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect(('127.0.0.1', 5000))
    while True:
        data = s.recv(1024).decode()
        if not data == 'done':
            msg = "<<STATE:1:2:3:4|REWARD:" + data + ">>"
            s.send(msg.encode())
        else:
            s.send("<<ACTION?>>".encode())
            s.send("<<DONE>>".encode())
            break
        """
        directory = os.path.abspath('build/worhp_ai')
        if not os.path.exists(directory):
            os.makedirs(directory)
        filename = os.path.join(directory, 'test_simple_communicator.py')
        with open(filename, "w") as file:
            file.write(prog)
        filename = os.path.join(directory, 'test_simple_communicator_run')
        with open(filename, "w") as file:
            file.write("#!/bin/bash \npython " + directory + "/test_simple_communicator.py")
        file_stat = os.stat(filename)
        os.chmod(filename, file_stat.st_mode | stat.S_IEXEC)

        # start communicator
        env = MarkovEnvCommunicator('127.0.0.1', 5000)
        env.start('test_simple_communicator_run')

        msg_data = [-1, 0, 30]
        for data in msg_data:
            env.send(data)
            msg = env.receive()
            self.assertEqual(msg[0][0], 'STATE')
            self.assertEqual(msg[0][1], [1.0, 2.0, 3.0, 4.0])
            self.assertEqual(msg[1][0], 'REWARD')
            self.assertEqual(msg[1][1], data)

        env.send('done')
        time.sleep(0.1)
        msg = env.receive()
        self.assertEqual(msg[0][0], 'ACTION?')
        self.assertEqual(msg[0][1], [])
        self.assertEqual(msg[1][0], 'DONE')
        self.assertEqual(msg[1][1], [])
        env.stop()


class TestMarkovAgentThread(unittest.TestCase):

    def test_markov_agent_thread_setup(self):
        par = {'threads': {'num': 2}}
        agent = Agent(None, None)
        agent_thread = MarkovAgentThread(agent, par)
        agent_thread.start()
        self.assertEqual(len(agent_thread.agent_result), 2)
        self.assertIsInstance(agent_thread.agent_thread, threading.Thread)
        self.assertTrue(agent_thread.agent_thread.is_alive())
        agent_thread.stop()
        self.assertFalse(agent_thread.agent_thread.is_alive())


    def test_markov_agent_thread_actions(self):

        class AgentTest(Agent):
            num_action_call = 0
            num_actionrand_call = 0
            num_train_call = 0
            num_pretrain_call = 0

            def action(self, state):
                self.num_action_call += 1
                return 42

            def action_rand(self, state):
                self.num_actionrand_call += 1
                return 43

            def train_episode(self, _):
                self.num_train_call += 1
                return 44

            def pretrain_episode(self, _):
                self.num_pretrain_call += 1
                return 45

        par = {'threads': {'num': 5}}
        agent = AgentTest(None, None)
        agent_thread = MarkovAgentThread(agent, par)
        agent_thread.start()
        self.assertEqual(agent.num_action_call, 0)
        self.assertEqual(agent.num_actionrand_call, 0)
        self.assertEqual(agent.num_train_call, 0)
        self.assertEqual(agent.num_pretrain_call, 0)
        for i in range(5):
            self.assertEqual(agent_thread.action(i, None), 42)
            self.assertFalse(agent_thread.agent_result[i])
            self.assertEqual(agent.num_action_call, i+1)
            self.assertEqual(agent.num_actionrand_call, i)
            self.assertEqual(agent.num_train_call, i)
            self.assertEqual(agent.num_pretrain_call, i)

            self.assertEqual(agent_thread.action_rand(i, None), 43)
            self.assertFalse(agent_thread.agent_result[i])
            self.assertEqual(agent.num_action_call, i+1)
            self.assertEqual(agent.num_actionrand_call, i+1)
            self.assertEqual(agent.num_train_call, i)
            self.assertEqual(agent.num_pretrain_call, i)

            self.assertEqual(agent_thread.train(i, None), 44)
            self.assertFalse(agent_thread.agent_result[i])
            self.assertEqual(agent.num_action_call, i+1)
            self.assertEqual(agent.num_actionrand_call, i+1)
            self.assertEqual(agent.num_train_call, i+1)
            self.assertEqual(agent.num_pretrain_call, i)

            self.assertEqual(agent_thread.pretrain(i, None), 45)
            self.assertFalse(agent_thread.agent_result[i])
            self.assertEqual(agent.num_action_call, i+1)
            self.assertEqual(agent.num_actionrand_call, i+1)
            self.assertEqual(agent.num_train_call, i+1)
            self.assertEqual(agent.num_pretrain_call, i+1)
        agent_thread.stop()


class TestMarkovLoop(unittest.TestCase):

    def test_markov_loop(self):
        class MarkovEnvCommunicatorTest(MarkovEnvCommunicator):

            def __init__(self, name, host, port):
                # pylint: disable=super-init-not-called

                self.name = name
                self.msg = [('STATE', [0.0]), ('ACTION?', []), ('REWARD', -0.0),
                            ('STATE', [1.0]), ('ACTION?', []), ('REWARD', -1.0),
                            ('STATE', [2.0]), ('ACTION?', []), ('REWARD', -2.0),
                            ('STATE', [3.0]), ('ACTION?', []), ('REWARD', -3.0),
                            ('STATE', [4.0]), ('ACTION?', []), ('REWARD', -4.0),
                            ('STATE', [5.0]), ('ACTION?', []), ('REWARD', -5.0),
                            ('STATE', [6.0]), ('ACTION?', []), ('REWARD', -6.0),
                            ('STATE', [7.0]), ('ACTION?', []), ('REWARD', -7.0),
                            ('STATE', [8.0]), ('ACTION?', []), ('REWARD', -8.0),
                            ('STATE', [9.0]), ('DONE', [])]
                self.msg_counter = -1

            def __del__(self):
                pass

            def send(self, action):
                pass

            def receive(self):
                self.msg_counter += 1
                return [self.msg[self.msg_counter]]


        class MarkovAgentCommunicatorTest(MarkovAgentCommunicator):
            action_counter = -1

            def train(self, thread_id, obs):
                pass

            def action(self, thread_id, state):
                self.action_counter += 1
                return 10 + self.action_counter

        # init
        env_comm = MarkovEnvCommunicatorTest('test', None, None)
        agent_comm = MarkovAgentCommunicatorTest(None, None, 0, None)
        par = {'learning': {'train_call': 'episode'},
               'training': {'state_recurrence': 0}}
        loop = MarkovLoop(env_comm, agent_comm, 0, 'training', 0, par)
        obs, _ = loop.run()

        self.assertEqual(len(obs), 10)
        for i in range(9):
            self.assertTrue('state' in obs[i])
            self.assertTrue('action' in obs[i])
            self.assertTrue('reward' in obs[i])
            self.assertTrue('next_state' in obs[i])
            self.assertTrue('terminal' in obs[i])
            self.assertEqual(obs[i]['state'], [[i]])
            self.assertEqual(obs[i]['action'], 10+i)
            self.assertEqual(obs[i]['next_state'], [[i+1]])
            self.assertEqual(obs[i]['reward'], -i)
            self.assertFalse(obs[i]['terminal'])
        self.assertTrue('state' in obs[9])
        self.assertFalse('action' in obs[9])
        self.assertFalse('reward' in obs[9])
        self.assertFalse('next_state' in obs[9])
        self.assertTrue('terminal' in obs[9])
        self.assertEqual(obs[9]['state'], [[9]])
        self.assertTrue(obs[9]['terminal'])


if __name__ == '__main__':
    unittest.main()
