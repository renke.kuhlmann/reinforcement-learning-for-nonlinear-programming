#!/usr/bin/env python3
# pylint: disable=missing-docstring

import unittest
from app.memory import Memory, MemoryMultiple

class TestMemory(unittest.TestCase):

    def test_memory_empty(self):
        mem = Memory(10)
        self.assertEqual(mem.size(), 0)


    def test_memory_max_size(self):
        mem = Memory(10)
        for i in range(1, 21):
            mem.add(i)
            if i < 10:
                self.assertTrue(mem.size() < 10)
            elif i >= 10:
                self.assertTrue(mem.size() == 10)


    def test_memory_delete_outdated(self):
        mem = Memory(10)
        for i in range(1, 21):
            mem.add(i)
            if i >= 10:
                for j in range(i-10+1, i+1):
                    self.assertTrue(j in mem.buffer)
                for j in range(1, i-10):
                    self.assertFalse(j in mem.buffer)


    def test_memory_sample_only_added(self):
        mem = Memory(10)
        for i in range(1, 11):
            mem.add(i)
        for i in range(100):
            batch = mem.sample(5)
            for elem in batch:
                self.assertTrue(1 <= elem <= 10)


class TestMemoryMultiple(unittest.TestCase):

    def test_memory_empty(self):
        mem = MemoryMultiple(10)
        self.assertEqual(mem.size(), 0)
        self.assertEqual(mem.size1("?"), 0)
        self.assertEqual(mem.size1("abc"), 0)


    def test_memory_added_classes(self):
        mem = MemoryMultiple(10)
        for i in range(1, 21):
            mem.add(str(i), 0)
        for name, _ in mem.idx.items():
            self.assertTrue(1 <= int(name) <= 20)


    def test_memory_max_size(self):
        mem = MemoryMultiple(10)
        for i in range(1, 21):
            mem.add('a', i)
            mem.add('b', i)
            if i < 10:
                self.assertTrue(mem.size1('a') < 10)
                self.assertTrue(mem.size1('b') < 10)
            elif i >= 10:
                self.assertTrue(mem.size1('a') == 10)
                self.assertTrue(mem.size1('b') == 10)


    def test_memory_delete_outdated(self):
        mem = MemoryMultiple(10)
        for i in range(1, 21):
            mem.add('a', i)
            mem.add('b', i)
            if i >= 10:
                for j in range(i-10+1, i+1):
                    self.assertTrue(j in mem.buffer_episodes[0])
                    self.assertTrue(j in mem.buffer_episodes[1])
                for j in range(1, i-10):
                    self.assertFalse(j in mem.buffer_episodes[0])
                    self.assertFalse(j in mem.buffer_episodes[1])


    def test_memory_sample_only_added(self):
        mem = MemoryMultiple(10)
        for i in range(1, 11):
            mem.add('a', i)
            mem.add('b', 10+i)
        for i in range(100):
            batch = mem.sample(5)
            for elem in batch:
                self.assertTrue(1 <= elem <= 20)


    def test_memory_get1(self):
        mem = MemoryMultiple(20)
        self.assertEqual(mem.get1('a'), None)
        self.assertEqual(mem.get1('b'), None)
        for i in range(1, 21):
            mem.add('a', i)
            mem.add('b', 20 + i)
        for item in mem.get1('a'):
            self.assertTrue(1 <= item <= 20)
        for item in mem.get1('b'):
            self.assertTrue(21 <= item <= 40)


    def test_memory_reset1(self):
        mem = MemoryMultiple(20)
        self.assertEqual(mem.get1('a'), None)
        self.assertEqual(mem.get1('b'), None)
        self.assertEqual(mem.size1('a'), 0)
        self.assertEqual(mem.size1('b'), 0)
        for i in range(1, 21):
            mem.add('a', i)
            mem.add('b', i)
        self.assertTrue(mem.size1('a') > 0)
        self.assertTrue(mem.size1('b') > 0)
        mem.reset1('a')
        self.assertEqual(mem.size1('a'), 0)
        self.assertTrue(mem.size1('b') > 0)


if __name__ == '__main__':
    unittest.main()
