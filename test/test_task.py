#!/usr/bin/env python3
# pylint: disable=missing-docstring

import unittest
from app.task import Task

class TestTask(unittest.TestCase):

    def test_task_empty_interface(self):
        task = Task({})

        # scaled
        states = [[], [1, 2, 3], [4.3, 75]]
        for state in states:
            stated_scaled = task.get_scaled_state(state)
            self.assertEqual(state, stated_scaled)

        # actions
        self.assertEqual(task.get_actions(state), [])


    def test_task_logscale(self):
        self.assertEqual(Task.logscale([1], 0, 2, 1), 0)
        self.assertEqual(Task.logscale([20], 0, 10, 1), 1)
        self.assertEqual(Task.logscale([0.1], 0, 1, 1), -1)
        self.assertEqual(Task.logscale([1e-10], 1e-08, 1, 1), -8)
        self.assertEqual(Task.logscale([1], 0, 1, 2), 0)
        self.assertEqual(Task.logscale([10], 0, 10, 2), 0.5)
        self.assertEqual(Task.logscale([10], 0, 10, 0.5), 2)

if __name__ == '__main__':
    unittest.main()
