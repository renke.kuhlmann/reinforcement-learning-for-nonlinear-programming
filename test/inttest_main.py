#!/usr/bin/env python3
# pylint: disable=missing-docstring

import os
import sys
import unittest
import stat
import shutil
from io import StringIO
import yaml

import app.__main__ as main

def create_test_prog():
    prog = """
#!/usr/bin/env python3
import sys
import socket
host1 = sys.argv[3]
port1 = int(sys.argv[5])
print(host1, port1)
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((host1, port1))
    for i in range(100):
        msg = "<<STATE:{0:d}:{1:d}:{2:d}:{3:d}:{4:d}:{5:d}:{6:d}|REWARD:{7:d}|ACTION?>>".format(i, i, i, i, i, i, i, -i)
        s.send(msg.encode())
        s.recv(1024).decode()
    msg = "<<STATE:{0:d}:{1:d}:{2:d}:{3:d}:{4:d}:{5:d}:{6:d}>>".format(100, 100, 100, 100, 100, 100, 100)
    s.send(msg.encode())
    s.send("<<DONE>>".encode())
    """
    directory = os.path.abspath('build/worhp_ai')
    if not os.path.exists(directory):
        os.makedirs(directory)
    filename = os.path.join(directory, 'integrationtest_1.py')
    with open(filename, "w") as file:
        file.write(prog)
    filename = os.path.join(directory, 'integrationtest_1')
    with open(filename, "w") as file:
        file.write("#!/bin/bash \npython " + directory + "/integrationtest_1.py $1 $2 $3 $4 $5")
    file_stat = os.stat(filename)
    os.chmod(filename, file_stat.st_mode | stat.S_IEXEC)

    directory = os.path.abspath('data/envs')
    filename = os.path.join(directory, 'integrationtests.txt')
    with open(filename, "w") as file:
        file.write("integrationtest_1\n")
        file.write("integrationtest_1\n")

    directory = os.path.abspath('data/baselines')
    filename = os.path.join(directory, 'integrationtest.csv')
    with open(filename, "w") as file:
        file.write("NAME;REWARD\n")
        file.write("integrationtest_1;-1.0\n")

def remove_test_prog():
    os.remove('build/worhp_ai/integrationtest_1.py')
    os.remove('build/worhp_ai/integrationtest_1')
    os.remove('data/envs/integrationtests.txt')
    os.remove('data/baselines/integrationtest.csv')
    os.remove('data/config/config_integrationtest.yml')


class TestMain(unittest.TestCase):

    def test_main_agent_static(self):
        # redirect stdout
        old_stdout = sys.stdout
        result = StringIO()
        sys.stdout = result

        create_test_prog()

        par = yaml.load(open('data/config/config_std.yml', 'r'))
        par['learning']['problems'] = 'integrationtests'
        par['learning']['agent'] = 'static'
        par['training']['num_prob'] = 1
        par['testing']['num_prob'] = 1
        par['performance']['baseline'] = 'integrationtest'
        par['training']['epochs'] = 2
        par['testing']['freq'] = 2
        yaml.dump(par, open('data/config/config_integrationtest.yml', 'w'))

        main.run(0, 'test', 'data/config/config_integrationtest.yml')
        main.run(1, 'test', 'data/config/config_integrationtest.yml')

        # check output
        created_files = next(os.walk('res/test'))[2]
        self.assertTrue(len(created_files) > 0)

        shutil.rmtree('res/test')
        remove_test_prog()

        # normal stdout
        sys.stdout = old_stdout

    def test_main_agent_q(self):
        # redirect stdout
        old_stdout = sys.stdout
        result = StringIO()
        sys.stdout = result

        create_test_prog()

        par = yaml.load(open('data/config/config_std.yml', 'r'))
        par['learning']['problems'] = 'integrationtests'
        par['learning']['agent'] = 'q'
        par['training']['num_prob'] = 1
        par['testing']['num_prob'] = 1
        par['performance']['baseline'] = 'integrationtest'
        par['training']['epochs'] = 2
        par['testing']['freq'] = 2
        yaml.dump(par, open('data/config/config_integrationtest.yml', 'w'))

        main.run(0, 'test', 'data/config/config_integrationtest.yml')
        main.run(1, 'test', 'data/config/config_integrationtest.yml')

        # check output
        created_files = next(os.walk('res/test'))[2]
        self.assertTrue(len(created_files) > 0)

        shutil.rmtree('res/test')
        remove_test_prog()

        # normal stdout
        sys.stdout = old_stdout

if __name__ == '__main__':
    unittest.main()
