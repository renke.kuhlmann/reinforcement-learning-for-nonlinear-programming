#!/usr/bin/env python3
"""
Markov Module

Implementation of markov decision process using external programs as environment
and communication using sockets. It basically gets a list of programs, executes
them in parallel, returns the observations and may call the agent in order to
train.
"""

# pylint: disable=too-many-lines

import threading
import time
import queue
import subprocess
import socket
from collections import deque
import numpy as np

import app.output as output
from app.stats import StatsEpisode, StatsEpoch


class Markov:
    """
    Markov Epoch

    An epoch is the execution of the Markov decision process for multiple
    environments. This class creates the agent thread, handles environment
    threads, collects observations and handles epoch training.

    Methods
    -------
    run(env_list, mode, epoch, stats_learn)
        Runs Epoch
    finalize()
        Stops process incl. join threads
    """

    # pylint: disable=too-many-instance-attributes

    def __init__(self, agent, par):
        """
        Parameters
        ---------
        agent : Agent
            Agent to be trained
        par : dict
            Parameters
        """

        # arguments
        self.agent = agent
        self.par = par

        # workspace
        self.mode = ''
        self.epoch = 0
        self.env_queue = queue.Queue()
        self.obs_queue = queue.Queue()
        self.stats_agent_queue = queue.Queue()

        # setup agent thread
        self.agent_thread = MarkovAgentThread(self.agent, self.par)
        if self.par['learning']['action_call'] == 'thread':
            self.agent_thread.start()

        # setup threads
        self.env_threads = []
        for i in range(self.par['threads']['num']):
            self.env_threads.append(MarkovEnvThread(self.agent, self.agent_thread,
                                                    self.env_queue, self.obs_queue,
                                                    self.stats_agent_queue, i,
                                                    self.par))
            self.env_threads[-1].start()


    def run(self, env_list, mode, epoch, stats_learn):
        """
        Runs Epoch

        Parameters
        ---------
        env_list : list
            Names of environments to be called
        mode : str
            Learning mode (e.g. training)
        epoch : int
            Current epoch number
        stats_learn : StatsLearn
            Statistics for learning process

        Returns
        -------
        list
            List of observations
        StatsEpoch
            Statistics of epoch run
        StatsAgent
            Statistics of agent
        """

        # pylint: disable=too-many-locals

        self.mode = mode
        self.epoch = epoch
        rewards_best = stats_learn.rewards_best

        # exploration rate
        expl_rate = self._exploration_rate()

        # epoch statistics
        stats_epo = StatsEpoch(mode, epoch, expl_rate)

        # output (header)
        output.print_epoch_header(mode, epoch)

        # init epoch for agent
        self.agent.init_epoch(self.epoch)

        # update environment threads
        for env_thread in self.env_threads:
            env_thread.update(self.mode)

        # fill environment queue
        for env in env_list:
            self.env_queue.put((env, rewards_best[env], expl_rate))

        # wait for jobs to be finished
        while self.env_queue.qsize() > 0 or self.obs_queue.qsize() < len(env_list):
            time.sleep(0.01)
            continue
        self.obs_queue.put(None)
        self.stats_agent_queue.put(None)

        # clear observation queue and store in list
        observations = []
        for obs in iter(self.obs_queue.get, None):
            observations.append(obs)

        # clear agent statistics queue and store in list
        stats_agent = []
        for stats in iter(self.stats_agent_queue.get, None):
            stats_agent.append(stats)

        # epoch training
        train_call = self.par['learning']['train_call']
        if train_call == 'epoch' and self.mode == 'pretraining':
            stats_agent = self.agent.pretrain_epoch(observations)
        if train_call == 'epoch' and self.mode == 'training':
            stats_agent = self.agent.train_epoch(observations)

        # finalize epoch for agent
        self.agent.finalize_epoch()

        # stop time
        stats_epo.time_stop()

        return observations, stats_epo, stats_agent


    def _exploration_rate(self):
        """
        Calculates exploration rate

        Returns
        -------
        float
            Exploration rate
        """

        # get parameters
        expl_start = self.par['training']['explore_start']
        expl_stop = self.par['training']['explore_stop']
        expl_decay = self.par['training']['explore_decay_rate']

        # probability
        prb = expl_stop + (expl_start - expl_stop) * np.exp(-expl_decay * self.epoch)

        if self.mode == 'training':
            return prb
        if self.mode == 'pretraining':
            return 1
        return 0


    def finalize(self):
        """
        Stops process incl. join threads
        """

        # stop agent thread
        if self.par['learning']['action_call'] == 'thread':
            self.agent_thread.stop()

        # add break up command
        for _ in range(self.par['threads']['num']):
            self.env_queue.put((None, None, None))

        # stop environment threads
        for env_thread in self.env_threads:
            env_thread.stop()



class MarkovAgentThread():
    """
    Agent Thread

    If multiple environments are called in parallel and request actions or
    training of the agent, this class can be used to handle requests. Therefore
    a single further thread is started.

    Methods
    -------
    start()
        Starts agent thread
    stop()
        Stops agent thread
    action(thread_id, state)
        Request agent action
    action_rand(thread_id, state)
        Request agent random action
    train(thread_id, obs)
        Request agent training
    pretrain(thread_id, obs)
        Request agent pretraining
    """

    def __init__(self, agent, par):
        """
        Parameters
        ---------
        agent : Agent
            Agent to be trained
        par : dict
            Parameters
        """

        # arguments
        self.agent = agent

        # workspace
        self.agent_thread = None
        self.agent_queue = queue.Queue()
        self.agent_stats = queue.Queue()
        self.agent_result = [None]*par['threads']['num']


    def start(self):
        """
        Starts agent thread
        """

        self.agent_thread = threading.Thread(target=self._thread)
        self.agent_thread.start()


    def stop(self):
        """
        Stops agent thread
        """

        self.agent_queue.put(None)
        self.agent_thread.join()


    def _thread(self):
        """
        Agent thread process

        Collects requests from a queue and process them. Results are stored in
        an array at the dimension of the called thread ID. The caller has to
        make sure, that results are read before more requests are send.
        """

        while True:
            # get task
            task = self.agent_queue.get()
            if task is None:
                break

            # read task
            task_type = task[0]
            task_pid = task[1]
            task_arg = task[2]

            # check if result space has been cleared
            if self.agent_result[task_pid]:
                print("WARNING: possible race condition")

            # perform task: action
            if task_type == "action":
                res = ("action", self.agent.action(task_arg))

            # perform task: random action
            elif task_type == "action_rand":
                res = ("action_rand", self.agent.action_rand(task_arg))

            # perform task: pretraining
            elif task_type == "pretrain_episode":
                train_stats = self.agent.pretrain_episode(task_arg)
                res = ("pretrain_episode", train_stats)

            # perform task: training
            elif task_type == "train_episode":
                train_stats = self.agent.train_episode(task_arg)
                res = ("train_episode", train_stats)

            else:
                print("ERROR: wrong task")
                return

            # store result
            self.agent_result[task_pid] = res


    def _request_thread(self, name, thread_id, data):
        """
        Add request to job queue

        Parameters
        ---------
        name : str
            Type of request (e.g. action or train_episode)
        thread_id : int
            Thread ID of calling thread
        data : object
            Data passed for the specific request

        Returns
        -------
        object
            Result of request
        """

        # add job and wait for result
        self.agent_queue.put((name, thread_id, data))
        while not self.agent_result[thread_id]:
            time.sleep(0.01)
            continue

        # clear result space
        result = self.agent_result[thread_id][1]
        self.agent_result[thread_id] = None

        return result


    def action(self, thread_id, state):
        """
        Request agent action

        Parameters
        ---------
        thread_id : int
            Thread ID of calling thread
        state : array_like
            Current state the action is based on

        Returns
        -------
        numeric
            Action of agent
        """

        return self._request_thread('action', thread_id, state)


    def action_rand(self, thread_id, state):
        """
        Request agent random action

        Parameters
        ---------
        thread_id : int
            Thread ID of calling thread
        state : array_like
            Current state the action is based on

        Returns
        -------
        numeric
            Random action of agent
        """

        return self._request_thread('action_rand', thread_id, state)


    def train(self, thread_id, obs):
        """
        Request agent training

        Parameters
        ---------
        thread_id : int
            Thread ID of calling thread
        obs : array_like
            Current observations the trainig step is based on

        Returns
        -------
        StatsAgent
            Statistics of agent
        """

        return self._request_thread('train_episode', thread_id, obs)


    def pretrain(self, thread_id, obs):
        """
        Request agent pretraining

        Parameters
        ---------
        thread_id : int
            Thread ID of calling thread
        obs : array_like
            Current observations the trainig step is based on

        Returns
        -------
        StatsAgent
            Statistics of agent
        """

        return self._request_thread('pretrain_episode', thread_id, obs)



class MarkovEnvThread():
    """
    Environment Thread

    Markov decision process for a given environment, which can be understood as
    episode. This process can run in parallel to other environments.

    Methods
    -------
    start()
        Starts environment thread
    update(mode)
        Updates data
    stop()
        Stops environment thread
    """

    # pylint: disable=too-many-instance-attributes

    def __init__(self, agent, agent_thread, env_queue, obs_queue, stats_queue, thread_id, par):
        """
        Parameters
        ---------
        agent : Agent
            Agent to be trained
        agent_thread : MarkovAgentThread
            Agent thread
        env_queue : Queue
            Job queue containing tuples of environment names and current best
            rewards
        obs_queue : Queue
            Result queue containing observations
        stats_queue : Queue
            Queue containing statistics of agent training
        thread_id : int
            Thread ID of calling thread
        par : dict
            Parameters
        """

        # pylint: disable=too-many-arguments

        self.agent = agent
        self.agent_thread = agent_thread
        self.env_queue = env_queue
        self.obs_queue = obs_queue
        self.stats_agent_queue = stats_queue
        self.thread_id = thread_id
        self.par = par

        self.mode = None
        self.agent_comm = None
        self.env_comm = None
        self.env_thread = None

        # get mini agent
        self.miniagent = self.agent.get_miniagent(self.thread_id)


    def start(self):
        """
        Starts environment thread
        """

        # get parameter
        host = self.par['threads']['host']
        port = self.par['threads']['ports1'] + self.thread_id

        # start environment communicator
        self.env_comm = MarkovEnvCommunicator(host, port)

        # start thread
        self.env_thread = threading.Thread(target=self._thread)
        self.env_thread.start()


    def update(self, mode):
        """
        Updates data

        Because Environment threads keep alive during the learning process and
        may run many different environments, data must be updated before the
        next run. This includes getting a fresh miniagent.

        Parameters
        ---------
        mode : str
            Learning mode (e.g. training)
        """

        self.mode = mode

        # start communicators and loop
        self.agent_comm = MarkovAgentCommunicator(self.agent, self.miniagent,
                                                  self.agent_thread, self.par)


    def stop(self):
        """
        Stops environment thread
        """

        self.env_thread.join()


    def _thread(self):
        """
        Environment thread process

        Runs the markov loop for a given environment collected from job queue.
        """

        while True:
            # get environment instance
            (env_name, best_reward, expl_rate) = self.env_queue.get()
            if env_name is None or best_reward is None:
                break

            # create environment run stats
            stats_eps = StatsEpisode(self.thread_id, env_name, best_reward)

            # start environment and run markov loop
            self.env_comm.start(env_name)
            loop = MarkovLoop(self.env_comm, self.agent_comm, self.thread_id,
                              self.mode, expl_rate, self.par)
            obs, stats_agent = loop.run()
            self.env_comm.stop()

            # get cumulative reward
            stats_eps.reward = 0
            for obs_single in obs:
                if 'reward' in obs_single:
                    stats_eps.reward += obs_single['reward']
            stats_eps.success = stats_eps.reward >= -self.par['nlp']['maxiter']

            # stop time
            stats_eps.time_stop()

            # add observations to queue
            self.obs_queue.put((env_name, obs))

            # add agent stats to queue
            self.stats_agent_queue.put((env_name, stats_agent))

            # output
            output.print_episode_statistic(stats_eps)



class MarkovAgentCommunicator():
    """
    Wrapper to either call main agent, miniagent or agent thread.

    Methods
    -------
    action(thread_id, state)
        Request agent action
    action_rand(thread_id, state)
        Request agent random action
    train(thread_id, obs)
        Request agent training
    pretrain(thread_id, obs)
        Request agent pretraining
    """

    def __init__(self, agent, miniagent, agent_thread, par):
        """
        Parameters
        ----------
        agent : Agent
            Agent to be trained
        miniagent : MiniAgent
            Miniagent belonging to agent
        agent_thread : MarkovAgentThread
            Agent thread
        par : dict
            Parameters
        """

        # arguments
        self.par = par
        self.agent = agent
        self.miniagent = miniagent
        self.agent_thread = agent_thread


    def action(self, thread_id, state):
        """
        Request agent action

        Parameters
        ---------
        thread_id : int
            Thread ID of calling thread
        state : array_like
            Current state the action is based on

        Returns
        -------
        numeric
            Action of agent
        """

        # single thread agent -> use global agent
        if self.par['threads']['num'] == 1:
            return self.agent.action(state)

        # multiple threads agent -> use agent via thread
        if self.par['learning']['action_call'] == 'thread':
            return self.agent_thread.action(thread_id, state)

        # miniagent
        return self.miniagent.action(state)


    def action_rand(self, thread_id, state):
        """
        Request agent random action

        Parameters
        ---------
        thread_id : int
            Thread ID of calling thread
        state : array_like
            Current state the action is based on

        Returns
        -------
        numeric
            Random action of agent
        """

        # single thread agent -> use global agent
        if self.par['threads']['num'] == 1:
            return self.agent.action_rand(state)

        # multiple threads agent -> use agent via thread
        if self.par['learning']['action_call'] == 'thread':
            return self.agent_thread.action_rand(thread_id, state)

        # miniagent
        return self.miniagent.action_rand(state)


    def train(self, thread_id, obs):
        """
        Request agent training

        Parameters
        ---------
        thread_id : int
            Thread ID of calling thread
        obs : array_like
            Current observations the trainig step is based on

        Returns
        -------
        StatsAgent
            Statistics of agent
        """

        # single thread agent -> use global agent
        if self.par['threads']['num'] == 1:
            return self.agent.train_episode(obs)

        # multiple threads agent -> use agent via thread
        if self.par['learning']['action_call'] == 'thread':
            return self.agent_thread.train(thread_id, obs)

        # miniagent
        # distributed training not implemented
        return 0


    def pretrain(self, thread_id, obs):
        """
        Request agent pretraining

        Parameters
        ---------
        thread_id : int
            Thread ID of calling thread
        obs : array_like
            Current observations the trainig step is based on

        Returns
        -------
        StatsAgent
            Statistics of agent
        """

        # single thread agent -> use global agent
        if self.par['threads']['num'] == 1:
            return self.agent.pretrain_episode(obs)

        # multiple threads agent -> use agent via thread
        if self.par['learning']['action_call'] == 'thread':
            return self.agent_thread.pretrain(thread_id, obs)

        # miniagent
        # distributed training not implemented
        return 0



class MarkovEnvCommunicator():
    """
    Environment Communicator

    Environments are considered to be implemented as independent programs.
    Communication with environment is established using sockets and a special
    protocol. This way states can be send and actions received within the
    environment execution at arbitrary locations.

    Methods
    -------
    start(instance)
        Starts environment
    receive()
        Receives messages from environment
    send(action)
        Send action to environment
    stop()
        Stops communication with environment
    """

    # pylint: disable=too-many-instance-attributes

    def __init__(self, host, port):
        """
        Parameters
        ----------
        host : str
            Socket host
        port : int
            Socket port
        """

        # parameter
        self.host = host
        self.port = port
        self.bufsize = 1024

        # create socket
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind((self.host, self.port))
        self.socket.listen()
        self.conn = None

        # process
        self.name = None
        self.proc = None

        # buffer
        self.buffer_part = ''


    def start(self, name):
        """
        Starts environment

        The executable the environment is implemented in gets started and the
        the method waits for the successful socket connection to the
        environment.

        Parameters
        ----------
        name : str
            Name of environment (external program)
        """

        self.name = name
        self.proc = subprocess.Popen(["build/worhp_ai/" + name,
                                      "--socket_enable",
                                      "--socket_host", self.host,
                                      "--socket_port", str(self.port)],
                                     stdout=subprocess.DEVNULL,
                                     stderr=subprocess.PIPE)
        self.conn, _ = self.socket.accept()


    def receive(self):
        """
        Receives messages from environment

        The socket connection is used to receive messages (e.g. current state).
        The message protocol is:
        <<caption1:content11:content12:...:content1n|caption2:content21:...>>
        Currently understood captions include STATE, REWARD, ACTION? and DONE.

        Returns
        -------
        list
            List of received messages
        """

        # receive buffer
        buf = self.buffer_part + self.conn.recv(self.bufsize).decode()

        # decode
        msgs_raw, self.buffer_part = self._decode_buffer(buf)

        # prepare data in messages
        msgs = []
        for msg in msgs_raw:
            if not msg:
                continue
            vals = msg.split(":")
            if vals[0] == "STATE":
                data = []
                for i in range(1, len(vals)):
                    data.append(float(vals[i]))
                msgs.append((vals[0], data))
            elif vals[0] == "REWARD":
                msgs.append((vals[0], float(vals[1])))
            elif vals[0] == "ACTION?":
                msgs.append((vals[0], []))
            elif vals[0] == "DONE":
                msgs.append((vals[0], []))
            else:
                print("ERROR: False message")
        return msgs


    @staticmethod
    def _decode_buffer(buffer):
        """
        Decodes the received buffer into single messages

        Parameters
        ----------
        buffer : str
            buffer of messages

        Returns
        list
            List of single messages
        """

        buffer_part = ''

        # decode messages
        msgs_full = []
        for msgs_tmp_1 in buffer.split("<<"):
            msgs_tmp_2 = msgs_tmp_1.split(">>")
            buffer_part = msgs_tmp_2.pop()
            for msgs_tmp_3 in msgs_tmp_2:
                if msgs_tmp_3:
                    msgs_full.append(msgs_tmp_3)

        # decode inner messages
        msgs_raw = []
        for msg in msgs_full:
            for msgs_tmp_1 in msg.split("|"):
                if msgs_tmp_1:
                    msgs_raw.append(msgs_tmp_1)

        return msgs_raw, buffer_part


    def send(self, action):
        """
        Send action to environment

        Parameters
        ----------
        action : int or float
            Index of action or action value
        """

        self.conn.send(str(action).encode())


    def stop(self):
        """
        Stops communication with environment
        """

        # process output
        _, err = self.proc.communicate()
        if err:
            print("ERROR: ", err)


    def __del__(self):
        # close socket
        self.socket.close()



class MarkovLoop():
    """
    Markov decision process for a given environment and agent communicator

    Methods
    -------
    run()
        Runs the Markov decision process
    """

    # pylint: disable=too-few-public-methods,too-many-instance-attributes

    def __init__(self, env_comm, agent_comm, thread_id, mode, expl_rate, par):
        """
        Parameters
        ----------
        env_comm : MarkovEnvCommunicator
            Environment Communicator
        agent_comm : MarkovAgentCommunicator
            Agent Communicator
        thread_id : int
            Thread ID of calling thread
        mode : str
            Learning mode (e.g. training)
        expl_rate : float
            Probability to explore (vs exploit)
        par : dict
            Parameters
        """

        # pylint: disable=too-many-arguments

        # arguments
        self.env_comm = env_comm
        self.agent_comm = agent_comm
        self.thread_id = thread_id
        self.mode = mode
        self.expl_rate = expl_rate
        self.par = par

        # workspace
        self.obs = []
        self.stats_agent = []
        self.state_buffer = deque(maxlen=self.par['training']['state_recurrence'])


    def run(self):
        """
        Runs the Markov decision process

        Gets messages from Environment communicator and process them, e.g.
        request agent training or agent action. Collected observations will be
        collected and returned.

        Returns
        -------
        list
            List of observations
        StatsAgent
            Statistics of agent
        """

        # init
        done = False

        # get and process messages
        while not done:
            for msg_head, msg_data in self.env_comm.receive():
                if msg_head == "DONE":
                    self._step_done()
                    done = True
                elif msg_head == "STATE":
                    self._step_state(msg_data)
                elif msg_head == "REWARD":
                    self._step_reward(msg_data)
                elif msg_head == "ACTION?":
                    self._step_action()

        return self.obs, self.stats_agent


    def _step_done(self):
        """
        Processes step for done message
        """

        self.obs[-1]['terminal'] = True

        # training wanted?
        if not self.par['learning']['train_call'] == 'episode' or len(self.obs) < 2:
            return

        # episode training
        if self.mode == 'pretraining':
            stats_agent = self.agent_comm.pretrain(self.thread_id, self.obs[-1])
            self.stats_agent.append(stats_agent)
        elif self.mode == 'training':
            stats_agent = self.agent_comm.train(self.thread_id, self.obs[-1])
            self.stats_agent.append(stats_agent)


    def _step_state(self, msg_data):
        """
        Processes step for state message

        Parameters
        ----------
        msg_data : object
            Message data
        """

        # check if new observation
        self._add_observation('state')

        # init state recurrence buffer
        if not self.state_buffer:
            for _ in range(self.state_buffer.maxlen):
                self.state_buffer.append(msg_data)

        # add state
        states = []
        for state_rec in self.state_buffer:
            states.append(state_rec)
        states.append(msg_data)

        if len(self.obs) >= 2:
            self.obs[-2]['next_state'] = states
        self.obs[-1]['state'] = states

        # add state to state recurrence buffer
        self.state_buffer.append(msg_data)


    def _step_reward(self, msg_data):
        """
        Processes step for reward message

        Parameters
        ----------
        msg_data : object
            Message data
        """

        # check if new observation
        self._add_observation('reward')

        # add reward
        self.obs[-1]['reward'] = msg_data


    def _step_action(self):
        """
        Processes step for action message
        """

        # check if new observation
        self._add_observation('action')

        # request action
        # pylint: disable=comparison-with-callable
        if self.expl_rate < np.random.rand():
            action = self.agent_comm.action(self.thread_id, self.obs[-1]['state'])
        else:
            action = self.agent_comm.action_rand(self.thread_id, self.obs[-1]['state'])

        # store action
        self.obs[-1]['action'] = action

        # send action
        self.env_comm.send(action)

        # training wanted?
        if not self.par['learning']['train_call'] == 'episode' or len(self.obs) < 2:
            return

        # episode training
        if self.mode == 'pretraining':
            stats_agent = self.agent_comm.pretrain(self.thread_id, self.obs[-2])
            self.stats_agent.append(stats_agent)
        elif self.mode == 'training':
            stats_agent = self.agent_comm.train(self.thread_id, self.obs[-2])
            self.stats_agent.append(stats_agent)


    def _add_observation(self, key):
        """
        Checks if a new observation in list is required
        """

        if not self.obs or key in self.obs[-1]:
            self.obs.append({'terminal': False, 'name': self.env_comm.name})
