#!/usr/bin/env python3
"""
Task Module

Provides classes that implement learning tasks. A task defines the state and
action space and may provide further utilities to modify states and actions.
"""

import numpy as np

class Task:
    """
    Task Interface

    Tasks define state and action spaces and provide utilities to convert and
    modify the state.

    Methods
    -------
    dim_rec_state()
        Returns dimension of recurrence state
    default_rec_state()
        Returns default recurrence state
    default_state()
        Returns default state
    get_scaled_flattened_rec_state(rec_state)
        Returns scaled and flattened recurrence state
    get_flattened_rec_state(rec_state)
        Returns flattened recurrence state
    get_scaled_state(state)
        Returns scaled state
    get_actions(state)
        Returns list of possible actions
    """

    def __init__(self, par):
        """
        Parameters
        ----------
        par : dict
            Parameters
        """

        self.par = par

        self.name = ''
        self.dim_states = 0
        self.dim_actions = 0
        self.type_states = ''
        self.type_actions = ''
        self.default_action = 0


    def dim_rec_state(self):
        """
        Returns dimension of recurrence state

        Returns
        -------
        int
            Dimension of recurrence state
        """

        return self.dim_states * (self.par['training']['state_recurrence'] + 1)


    def default_rec_state(self):
        """
        Returns default recurrence state

        Returns
        -------
        array_like
            Default state
        """

        rec_state = []
        for _ in range(self.par['training']['state_recurrence'] + 1):
            rec_state.append(self.default_state())
        return rec_state


    def default_state(self):
        """
        Returns default state

        Returns
        -------
        array_like
            Default state
        """

        return [0]*self.dim_states


    def get_scaled_flattened_rec_state(self, rec_state):
        """
        Returns scaled and flattened recurrence state

        Parameters
        ----------
        rec_state : array_like
            Recurrence state (i.e. list of states)

        Returns
        -------
        array_like
            Flattened and scaled state
        """

        scaled_rec_state = []
        for state in rec_state:
            scaled_rec_state.append(self.get_scaled_state(state))
        return self.get_flattened_rec_state(scaled_rec_state)


    def get_flattened_rec_state(self, rec_state):
        """
        Returns flattened recurrence state

        Parameters
        ----------
        rec_state : array_like
            Recurrence state (i.e. list of states)

        Returns
        -------
        array_like
            Flattened state
        """

        # pylint: disable=no-self-use

        return np.array(rec_state).flatten()


    def get_scaled_state(self, state):
        """
        Returns scaled state

        Parameters
        ----------
        state : array_like
            State to be scaled

        Returns
        -------
        array_like
            Scaled state
        """

        # pylint: disable=no-self-use

        return state


    def get_actions(self, state):
        """
        Returns list of possible actions

        For discrete action space the return is assumed to be a list of indices
        of allowed actions, e.g. [0, 1, 3]. For continuous action space to be a
        list of tuples (low,up) with lower and upper bounds, e.g.
        [(0, 1), (-1,1)].

        Parameters
        ----------
        state : array_like
            State to be scaled

        Returns
        -------
        list
            List of possible actions
        """

        # pylint: disable=no-self-use,unused-argument

        return []


    @staticmethod
    def logscale(arr, minval, maxval, scaling):
        """
        Log Scaling with min-max clip

        Parameters
        ----------
        arr : array_like
            Values to be scaled
        minval : float
            Minimum value to be projected to
        maxval : float
            Maximum value to be projected to
        scaling : float
            Scaling after log-scaling

        Returns
        -------
        float
            Scaled Values
        """

        return np.log10(np.minimum(np.maximum(np.array(arr), minval), maxval)) / scaling


class TaskBarrier(Task): # pragma: no cover
    """
    Steering Barrier Parameter Task

    State space is a combination of KKT conditions of original and barrier
    problem, min and mean complementarity value and quality functions for each
    possible action. Action space consists of five actions (none, slow and fast
    decrease, slow and fast increase). The definitions in this class must meet
    the implementation of the environment (external program).

    Methods
    -------
    dim_rec_state()
        Returns dimension of recurrence state
    default_rec_state()
        Returns default recurrence state
    default_state()
        Returns default state
    get_scaled_flattened_rec_state(rec_state)
        Returns scaled and flattened recurrence state
    get_flattened_rec_state(rec_state)
        Returns flattened recurrence state
    get_scaled_state(state)
        Returns scaled state
    get_actions(state)
        Returns list of possible actions
    get_barrier(state)
        Returns the barrier from state array
    get_kkt(state)
        Returns the KKT error of original problem from state array
    get_kkt_bar(state)
        Returns the KKT error of barrier problem from state array
    """

    # pylint: disable=too-many-instance-attributes

    def __init__(self, par):
        Task.__init__(self, par)

        self.name = 'barrier'
        self.dim_states = 7
        self.dim_actions = 5
        self.type_states = 'ddddd'
        self.type_actions = 'i'
        self.default_action = 0

        self.barrier_min = 1e-12
        self.barrier_max = 1e0


    def get_scaled_state(self, state):
        """
        Returns scaled state

        Parameters
        ----------
        state : array_like
            State to be scaled

        Returns
        -------
        array_like
            Scaled state
        """

        # scale different values
        barr = self.logscale(state[0], 1e-20, 1e+20, 12.0)
        opti = self.logscale(state[1], 1e-08, 1e+08, 6.0)
        feas = self.logscale(state[2], 1e-08, 1e+08, 6.0)
        comp = self.logscale(state[3], 1e-08, 1e+08, 6.0)
        comp_barr = self.logscale(state[4], 1e-08, 1e+08, 6.0)
        comp_min = self.logscale(state[5], 1e-08, 1e+08, 6.0)
        comp_mean = self.logscale(state[6], 1e-08, 1e+08, 6.0)

        # concat
        state = [barr, opti, feas, comp, comp_barr, comp_min, comp_mean]

        return state


    def get_actions(self, state):
        """
        Returns list of possible actions

        If barrier parameter is at its maximum value a further increase is not
        possible. Similar for the minimum value.

        Parameters
        ----------
        state : array_like
            State to be scaled

        Returns
        -------
        list
            List of possible actions
        """

        barr = TaskBarrier.get_barrier(state)
        if barr <= self.barrier_min:
            return [0, 2, 4]
        if barr >= self.barrier_max:
            return [0, 1, 3]
        return [0, 1, 2, 3, 4]


    @staticmethod
    def get_barrier(state):
        """
        Returns the barrier from state array

        Parameters
        ----------
        state : array_like
            State to be scaled

        Returns
        -------
        float
            Barrier parameter
        """

        return state[0]


    @staticmethod
    def get_kkt(state):
        """
        Returns the KKT error of original problem from state array

        Parameters
        ----------
        state : array_like
            State to be scaled

        Returns
        -------
        float
            KKT error of original problem
        """

        return np.max([state[1:3]])


    @staticmethod
    def get_kkt_bar(state):
        """
        Returns the KKT error of barrier problem from state array

        Parameters
        ----------
        state : array_like
            State to be scaled

        Returns
        -------
        float
            KKT error of barrier problem
        """

        return np.max([state[1], state[2], state[4]])
