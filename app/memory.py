#!/usr/bin/env python3
"""
Memory Module

Provides various Memory implementations for experience replay.
"""

from collections import deque, Counter
import numpy as np

class Memory():
    """
    A simple Experience Memory

    Stores experiences in a buffer. If an experience is added when the memory
    already reached the maximum size, the oldest experience is deleted.

    Methods
    -------
    add(experience)
        Adds an experience to memory
    sample(batch_size)
        Samples experiences from memory
    size()
        Returns the size of memory
    """

    def __init__(self, max_size=1000):
        """
        Parameters
        ----------
        max_size : int
            Max size of memory
        """

        self.max_size = max_size
        self.buffer = deque(maxlen=max_size)


    def add(self, experience):
        """
        Adds an experience to memory

        Parameters
        ----------
        experience : object
            Experience to be added
        """

        self.buffer.append(experience)


    def sample(self, batch_size):
        """
        Samples experiences from memory

        Parameters
        ----------
        batch_size : int
            Size of sample

        Returns
        -------
        list
            Sample of experiences
        """

        # pylint: disable=not-an-iterable
        idx = np.random.choice(np.arange(len(self.buffer)), size=batch_size,
                               replace=False)
        return [self.buffer[ii] for ii in idx]


    def size(self):
        """
        Returns the size of memory

        Returns
        -------
        int
            Size of memory
        """

        return len(self.buffer)


class MemoryMultiple():
    """
    An Experience Memory with multiple classes

    It is assumed that experiences belong to a given class and are stored
    together with other experiences from the same class. If an experience is
    added when the class memory already reached the maximum size, the oldest
    experience is deleted.

    Methods
    -------
    add(name, experience)
        Adds an experience to class memory
    sample(batch_size)
        Samples experiences from all class memories
    size(name)
        Returns the number of classes
    size1(name)
        Returns the size of given class memory
    """

    def __init__(self, max_size=1000):
        """
        Parameters
        ----------
        max_size : int
            Max size of each class memory
        """

        self.max_size = max_size
        self.idx = {}
        self.buffer_episodes = []


    def add(self, name, experience):
        """
        Adds an experience to class memory

        Parameters
        ----------
        name : str
            Name of class
        experience : object
            Experience to be added
        """

        if not name in self.idx:
            self.idx[name] = len(self.buffer_episodes)
            self.buffer_episodes.append(deque(maxlen=self.max_size))
        self.buffer_episodes[self.idx[name]].append(experience)


    def sample(self, batch_size):
        """
        Samples experiences from all class memories

        Samples are generated from all class memories with equal probability.

        Parameters
        ----------
        batch_size : int
            Size of sample

        Returns
        -------
        list
            Sample of experiences
        """

        # pylint: disable=not-an-iterable
        idx = np.random.choice(np.arange(len(self.buffer_episodes)),
                               size=batch_size, replace=True)
        counter = Counter(idx)
        sample = []
        for i, num in counter.items():
            idx2 = np.random.choice(np.arange(len(self.buffer_episodes[i])),
                                    size=num, replace=False)
            for j in idx2:
                sample.append(self.buffer_episodes[i][j])
        return sample


    def get1(self, name):
        """
        Returns memory of given class

        Parameters
        ----------
        name : str
            Name of class

        Returns
        -------
        deque
            Memory of given class
        """

        if not name in self.idx:
            return None
        return self.buffer_episodes[self.idx[name]]


    def reset1(self, name):
        """
        Resets memory of given class

        Parameters
        ----------
        name : str
            Name of class
        """

        self.buffer_episodes[self.idx[name]] = deque(maxlen=self.max_size)


    def size(self):
        """
        Returns the number of classes

        Returns
        -------
        int
            Number of classes
        """

        return len(self.idx)


    def size1(self, name):
        """
        Returns the size of given class memory

        Parameters
        ----------
        name : str
            Name of class

        Returns
        -------
        int
            Size of class memory
        """

        if not name in self.idx:
            return 0
        return len(self.buffer_episodes[self.idx[name]])
