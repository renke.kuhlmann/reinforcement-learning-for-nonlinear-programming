#!/usr/bin/env python3
"""
Statistic Module

Collection of classes that stores information for various aspects, e.g. run of
episode or run of epoch.
"""

import time
import numpy as np

class Stats:
    """
    Base class for Statistics

    Methods
    -------
    stop_time()
        Stops timer
    """

    # pylint: disable=too-few-public-methods


    def __init__(self):
        self.time0 = 0.0
        self.time = 0.0
        self.time_start()


    def time_start(self):
        """
        Starts timer
        """

        self.time0 = time.time()


    def time_stop(self):
        """
        Stops timer
        """

        self.time += time.time() - self.time0


    def time_reset(self):
        """
        Resets timer
        """

        self.time0 = 0.0
        self.time = 0.0


class StatsEpisode(Stats):
    """
    Statistics for Episode Run
    """

    # pylint: disable=too-few-public-methods


    def __init__(self, thread_id, name, reward_best):
        """
        Parameters
        ----------
        thread_id : int
            ID of used thread
        name : str
            Environment name
        reward_best : float
            Best reward of environment so far
        """

        self.thread_id = thread_id
        self.name = name
        self.reward = None
        self.reward_best = reward_best
        self.success = False
        Stats.__init__(self)


class StatsEpoch(Stats):
    """
    Statistics for Epoch Run
    """

    # pylint: disable=too-few-public-methods


    def __init__(self, mode, epoch, explore_p):
        """
        Parameters
        ----------
        mode : str
            Learning mode, e.g. training
        epoch : int
            Epoch number
        explore_p : float
            Exploration rate
        """

        self.mode = mode
        self.epoch = epoch
        self.explore_p = explore_p
        self.performance = None
        self.performance_best = None
        Stats.__init__(self)


class StatsLearn(Stats):
    """
    Statistics for Epoch Run

    Methods
    -------
    reward_reset()
        Resets the rewards
    performance_reset()
        Resets the performance measure
    update_epoch(epoch, rewards, performance)
        Updates rewards and performances of current epoch
    get_performance_mean_std(tail)
        Computes mean, standard deviation and median of performances
    get_performance_best()
        Best performance
    """

    def __init__(self, names):
        self.names = names
        self.rewards = {}
        self.rewards_best = {}
        self.epochs = []
        self.performances = []
        self.performance_best = None
        Stats.__init__(self)

        # init rewards
        self.reward_reset()
        self.performance_reset()


    def reward_reset(self):
        """
        Resets the rewards
        """

        for name in self.names:
            self.rewards[name] = -float("inf")
            self.rewards_best[name] = -float("inf")


    def performance_reset(self):
        """
        Resets the performance measure
        """

        self.performances = []
        self.performance_best = -float("inf")


    def update_epoch(self, epoch, rewards, performance):
        """
        Updates rewards and performances of current epoch

        Parameters
        ----------
        epoch : int
            Number of epoch
        rewards : dict
            Dictionary (name of environment -> reward of environment)
        performance : float
            Performance measure of epoch
        """

        # store epoch
        self.epochs.append(epoch)

        # store rewards
        for _, (name, reward) in enumerate(rewards.items()):
            if reward >= self.rewards_best[name]:
                self.rewards_best[name] = reward
            self.rewards[name] = reward

        # store performance
        self.performances.append(performance)
        if performance >= self.performance_best:
            self.performance_best = performance


    def get_performance_mean_std(self, tail):
        """
        Computes mean, standard deviation and median of performances

        Parameters
        ----------
        tail : float
            Percentage of tail performances to be considered

        Returns
        -------
        float
            Mean
        float
            Standard deviation
        float
            Median
        """

        idx = int(np.floor(len(self.performances) * (1 - tail)))
        mean = np.mean(self.performances[idx:])
        std = np.std(self.performances[idx:])
        median = np.median(self.performances[idx:])
        return mean, std, median


    def get_performance_best(self):
        """
        Best performance

        Returns
        -------
        int
            Epoch of best performance
        float
            Best performance
        """

        idx = self.epochs[np.argmax(self.performances)]
        value = np.max(self.performances)
        return idx, value


class StatsAgent(Stats):
    """
    Statistics for Agent Train Step
    """

    # pylint: disable=too-few-public-methods

    def __init__(self):
        Stats.__init__(self)
