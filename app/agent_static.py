#!/usr/bin/env python3
"""
Static Agent
"""

from app.agent import Agent, MiniAgent
from app.task import TaskBarrier

class AgentStatic(Agent):
    """
    Static Agent

    The static agent does not learn, but implements a reference policy for the
    given task. For example for the barrier update policy the monotone Fiacco-
    McCormick barrier update is implemented.

    Methods
    -------
    restart()
        Starts agent to resume training
    action(state)
        Selection of action (exploitation)
    action_rand(state)
        Selection of action (exploration)
    pretrain_envs(envs_all)
        Specifies environments for pretraining
    pretrain_episode(env, state, action, reward, next_state, terminal)
        Performs pretraining (called at end of episode)
    pretrain_epoch(observations)
        Performs pretraining (called at end of epoch)
    train_episode(env, state, action, reward, next_state, terminal)
        Performs pretraining (called at end of episode)
    train_epoch(observations)
        Performs pretraining (called at end of epoch)
    get_miniagent(id)
        Creates Mini Agent
    current_best(mode)
        Indicates that current learning status is best so far
    finalize(self)
        Prepares the agent for storage
    """

    def action(self, state):
        """
        Selection of action (exploitation)

        Parameters
        ----------
        state : array_like
            State the action is based on

        Returns
        -------
        int
            Index of action
        """

        if not isinstance(self.task, TaskBarrier):
            print("ERROR: Task not implemented!")
            return 0

        # standard monotone fiacco-mccormick barrier update
        if self.task.get_kkt_bar(state[-1]) <= 10 * self.task.get_barrier(state[-1]):
            return 1
        return 0


    def get_miniagent(self, thread_id):
        """
        Creates Mini Agent

        The mini agent has limited features, which allows to take actions based
        on the current learning status, but does not allow training.

        Parameters
        ----------
        thread_id : int
            ID of thread that requires a copy for evaluation

        Returns
        -------
        MiniAgentStatic
            Mini agent
        """

        # pylint: disable=unused-argument

        return MiniAgentStatic(self.task)


class MiniAgentStatic(MiniAgent):
    """
    Mini Agent of Static Agent

    The mini agent has limited features, which allows to take actions based on
    the current learning status, but does not allow training.

    Methods
    -------
    action(state)
        Selection of action (exploitation)
    action_rand(state)
        Selection of action (exploration)
    """

    # pylint: disable=too-few-public-methods


    def __init__(self, task):
        """
        Parameters
        ----------
        task : Task
            Specified task to be learned (e.g., defines state and action spaces)
        """

        MiniAgent.__init__(self, task)


    def action(self, state):
        """
        Selection of action (exploitation)

        Parameters
        ----------
        state : array_like
            State the action is based on

        Returns
        -------
        int
            Index of action
        """

        if self.task.get_kkt_bar(state[-1]) <= 10 * self.task.get_barrier(state[-1]):
            return 1
        return 0
