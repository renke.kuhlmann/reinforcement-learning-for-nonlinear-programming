#!/usr/bin/env python3
"""
Agent Interface
"""

import numpy as np

from app.stats import StatsAgent

class Agent:
    """
    Agent Interface

    Methods
    -------
    restart()
        Starts agent to resume training
    init_epoch(epoch)
        Initializes epoch
    finalize_epoch()
        Finalizes epoch
    action(state)
        Selection of action (exploitation)
    action_rand(state)
        Selection of action (exploration)
    pretrain_envs(envs_all)
        Specifies environments for pretraining
    pretrain_episode(observation)
        Performs pretraining (called at end of episode)
    pretrain_epoch(observations)
        Performs pretraining (called at end of epoch)
    train_episode(observation)
        Performs pretraining (called at end of episode)
    train_epoch(observations)
        Performs pretraining (called at end of epoch)
    get_miniagent(id)
        Creates Mini Agent
    current_best(mode)
        Indicates that current learning status is best so far
    update_regret_ref(ref_rewards)
        Updates the reference reward (best reward) in regret calculation
    finalize(self)
        Prepares the agent for storage
    """

    def __init__(self, task, par):
        """
        Parameters
        ----------
        task : Task
            Specified task to be learned (e.g., defines state and action spaces)
        par : dict
            Parameters
        """

        self.task = task
        self.par = par

        self.ref_rewards = {}


    def restart(self, par):
        """
        Starts agent to resume training

        This method must be called after previous agent data has been recovered
        and before further training.

        Parameters
        ----------
        par : dict
            Parameters
        """


    def init_epoch(self, epoch):
        """
        Initializes epoch

        Parameters
        ----------
        epoch : int
            Current epoch number
        """


    def finalize_epoch(self):
        """
        Finalizes epoch
        """


    def action(self, state):
        """
        Selection of action (exploitation)

        Parameters
        ----------
        state : array_like
            State the action is based on

        Returns
        -------
        int
            Index of action
        """

        # pylint: disable=unused-argument,no-self-use

        return self.task.default_action


    def action_rand(self, state):
        """
        Selection of action (exploration)

        Parameters
        ----------
        state : array_like
            State the action is based on

        Returns
        -------
        int
            Index of action
        """

        # pylint: disable=unsubscriptable-object
        idx = self.task.get_actions(state[-1])
        return np.random.choice(idx, 1)[0]


    def pretrain_envs(self, envs_all):
        """
        Specifies environments for pretraining

        Parameters
        ----------
        envs_all : list
            List of all considered environments

        Returns
        -------
        list
            List of environments for pretraining
        """

        # pylint: disable=unused-argument,no-self-use

        return []


    def pretrain_episode(self, observation):
        """
        Performs pretraining (called at end of episode)

        Parameters
        ----------
        Dictionary of observations (keys: name, state, action, reward,
            next_state, terminal)
        """


    def pretrain_epoch(self, observations):
        """
        Performs pretraining (called at end of epoch)

        Parameters
        ----------
        observations : list
            List of collected observations for multiple environments.
        """


    def train_episode(self, observation):
        """
        Performs training (called at end of episode)

        Parameters
        ----------
        Dictionary of observations (keys: name, state, action, reward,
            next_state, terminal)

        Returns
        -------
        StatsAgent
            Agent train stats
        """

        # pylint: disable=unused-argument,no-self-use

        stats_agent = StatsAgent()
        stats_agent.time_stop()
        return stats_agent


    def train_epoch(self, observations):
        """
        Performs training (called at end of epoch)

        Parameters
        ----------
        observations : list
            List of collected observations for multiple environments.

        Returns
        -------
        StatsAgent
            Agent train stats
        """

        # pylint: disable=unused-argument,no-self-use

        stats_agent = StatsAgent()
        stats_agent.time_stop()
        return stats_agent


    def get_miniagent(self, thread_id):
        """
        Creates Mini Agent

        The mini agent has limited features, which allows to take actions based
        on the current learning status, but does not allow training.

        Parameters
        ----------
        thread_id : int
            ID of thread that requires a copy for evaluation

        Returns
        -------
        MiniAgent
            Mini agent
        """

        # pylint: disable=unused-argument

        return MiniAgent(self.task)


    def current_best(self, mode):
        """
        Indicates that current learning status is best so far

        The learning status is measured in different modes (e.g. training,
        testing_training, testing_testing) to specify the set of environments
        the agent befors better than before.

        Parameters
        ----------
        mode : str
            Mode the best performance has been encountered in (e.g. training)
        """


    def update_regret_ref(self, ref_rewards):
        """
        Updates the reference reward (best reward) in regret calculation

        Parameters
        ----------
        ref_rewards : dict
            Dictionary with key: name of environment and value: reference reward
        """

        for name, ref_reward in ref_rewards.items():
            if not name in self.ref_rewards:
                self.ref_rewards[name] = ref_reward
            else:
                self.ref_rewards[name] = max(self.ref_rewards[name], ref_reward)


    def finalize(self):
        """
        Prepares the agent for storage

        This method has to be called before the agent data will be stored. Since
        Pickle has trouble with TensorFlow data, TensorFlow will store the
        neural network itself. Furthermore, all references to the neural
        network, TensorFlow servers and sessions will be deleted.
        """


class MiniAgent:
    """
    Mini Agent Interface

    The mini agent has limited features, which allows to take actions based on
    the current learning status, but does not allow training.

    Methods
    -------
    action(state)
        Selection of action (exploitation)
    action_rand(state)
        Selection of action (exploration)
    """

    def __init__(self, task):
        """
        Parameters
        ----------
        task : Task
            Specified task to be learned (e.g., defines state and action spaces)
        """

        self.task = task


    def action(self, state):
        """
        Selection of action (exploitation)

        Parameters
        ----------
        state : array_like
            State the action is based on

        Returns
        -------
        int
            Index of action
        """

        # pylint: disable=unused-argument,no-self-use

        return self.task.default_action


    def action_rand(self, state):
        """
        Selection of action (exploration)

        Parameters
        ----------
        state : array_like
            State the action is based on

        Returns
        -------
        int
            Index of action
        """

        # pylint: disable=unsubscriptable-object
        idx = self.task.get_actions(state[-1])
        return np.random.choice(idx, 1)[0]
