#!/usr/bin/env python3
"""
A Deep Q-Learning Agent
"""

import os
import numpy as np
import tensorflow as tf

from app.agent import Agent, MiniAgent
from app.memory import MemoryMultiple
from app.stats import StatsAgent

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

# pylint: disable=no-member

class AgentQ(Agent):
    """
    A Deep Q-Learning Agent

    The current implementation assumes a continuous state and discrete action
    space. The state-action value function Q is approximated using a neural
    network. Deep Q-Learning extension that are utilized are experience replay,
    target Q-network and regret formulation. Distributed TensorFlow is used to
    handle the neural network, which allows the parallel excecution of
    collecting observations. The training however is performed at the master
    thread alone.

    Methods
    -------
    restart()
        Starts agent to resume training
    init_epoch(epoch)
        Initializes epoch
    finalize_epoch()
        Finalizes epoch
    action(state)
        Selection of action (exploitation)
    action_rand(state)
        Selection of action (exploration)
    pretrain_envs(envs_all)
        Specifies environments for pretraining
    pretrain_episode(env, state, action, reward, next_state, terminal)
        Performs pretraining (called at end of episode)
    pretrain_epoch(observations)
        Performs pretraining (called at end of epoch)
    train_episode(env, state, action, reward, next_state, terminal)
        Performs pretraining (called at end of episode)
    train_epoch(observations)
        Performs pretraining (called at end of epoch)
    get_miniagent(id)
        Creates Mini Agent
    current_best(mode)
        Indicates that current learning status is best so far
    update_regret_ref(ref_rewards)
        Updates the reference reward (best reward) in regret calculation
    finalize_tensorflow()
        Finalizes Tensorflow
    finalize()
        Prepares the agent for storage
    """

    # pylint: disable=too-many-instance-attributes

    def __init__(self, task, par):
        """
        Parameters
        ----------
        task : Task
            Specified task to be learned (e.g., defines state and action spaces)
        par : dict
            Parameters
        """

        # arguments
        self.par = par

        # tensorflow attributes
        self.server = None
        self.sess = None
        self.net = {}

        # init parent
        Agent.__init__(self, task, par)

        # start tensorflow session
        self._session_start()

        # init neural network
        self._neural_network_init()
        self._neural_network_ref()
        self.sess.run(self._neural_network_update_target(1))
        frac = par['agent_q']['target_update_frac']
        self.update_target_op = self._neural_network_update_target(frac)

        # memory
        self.memory_episode = MemoryMultiple(self.par['agent_q']['memory_size'])
        self.memory = MemoryMultiple(self.par['agent_q']['memory_size'])

        # counter
        self.epoch = 0
        self.num_train = 0
        self.num_train_epoch_start = 0
        self.target_update_freq = self.par['agent_q']['target_update_freq_train']


    def restart(self, par):
        """
        Starts agent to resume training

        This method must be called after previous agent data has been recovered
        and before further training. The method needs to load Tensorflow network
        data, since storage is not handled by pickle.

        Parameters
        ----------
        par : dict
            Parameters
        """

        self.par = par

        # get parameters
        agent_dir = self.par['dir']['agent']

        # start tensorflow session
        self._session_start()

        # load neural network
        saver = tf.train.import_meta_graph(os.path.join(agent_dir, 'graph.meta'))
        saver.restore(self.sess, tf.train.latest_checkpoint(agent_dir))
        self._neural_network_ref()

        # restart neural network
        frac = par['agent_q']['target_update_frac']
        self.update_target_op = self._neural_network_update_target(frac)


    def _session_start(self):
        """
        Starts TensorFlow Master Session

        To allow for parallel evaluations of the neural network, a TensorFlow
        cluster is initialized and servers are started. For the master thread
        (which is responsible e.g. for training) the TensorFlow session gets
        started. Needs to be called at initialization or reload.
        """

        # get parameters
        threads_num = self.par['threads']['num']
        host = self.par['threads']['host']
        port = self.par['threads']['ports2']

        # setup tensorflow cluster (one session for master and one for each
        # possible environment thread)
        tasks = []
        for i in range(threads_num+1):
            tasks.append(host + ":" + str(port+i))
        cluster = tf.train.ClusterSpec({"local": tasks})

        # start master server
        self.server = []
        for i in range(threads_num+1):
            self.server.append(tf.train.Server(cluster, job_name="local", task_index=i))

        # start session
        self.sess = tf.Session(target=self.server[-1].target)


    def init_epoch(self, epoch):
        """
        Initializes epoch

        Parameters
        ----------
        epoch : int
            Current epoch number
        """

        self.epoch = epoch
        self.num_train_epoch_start = self.num_train


    def finalize_epoch(self):
        """
        Finalizes epoch
        """

        # determine target update frequency
        freq = int((self.num_train - self.num_train_epoch_start) /
                   self.par['training']['num_prob'])
        if freq > 0:
            self.target_update_freq = freq


    def action(self, state):
        """
        Selection of action (exploitation)

        Parameters
        ----------
        state : array_like
            State the action is based on

        Returns
        -------
        int
            Index of action
        """

        state_in = np.array(self.task.get_scaled_flattened_rec_state(state))
        feed_dict = {self.net['inputs']: state_in.reshape((1, *state_in.shape))}
        q_values = self.sess.run(self.net['output'], feed_dict=feed_dict)
        return np.argmax(q_values[0])


    def pretrain_envs(self, envs_all):
        """
        Specifies environments for pretraining

        In order to guarantee that experience replay buffer has at least a
        minimum number of observations of each environment, pretraining on these
        is executed. This method specifies the environments which require
        further observations before training begins. If an empty list is
        returned, pretraining stops or is not even started.

        Parameters
        ----------
        envs_all : list
            List of all considered environments

        Returns
        -------
        envs_pretrain : list
            List of environments for pretraining
        """

        pretrain_list = []
        for name in envs_all:
            if self.memory.size1(name) <= self.par['agent_q']['memory_size_min']:
                pretrain_list.append(name)
        return pretrain_list


    def pretrain_episode(self, observation):
        """
        Performs pretraining (called at end of episode)

        Parameters
        ----------
        observation : dict
            Dictionary of observations (keys: name, state, action, reward,
            next_state, terminal)
        """

        # add observation to memory
        self._store_observations([(observation['name'], [observation])])


    def pretrain_epoch(self, observations):
        """
        Performs pretraining (called at end of epoch)

        Parameters
        ----------
        observations : list
            List of collected observations for multiple environments.
        """

        # add observations to memory
        self._store_observations(observations)


    def train_episode(self, observation):
        """
        Performs training (called at end of episode)

        Parameters
        ----------
        observation : dict
            Dictionary of observations (keys: name, state, action, reward,
            next_state, terminal)

        Returns
        -------
        StatsAgent
            Agent train stats
        """

        # add observation to memory
        self._store_observations([(observation['name'], [observation])])

        # train once
        return self._train()


    def train_epoch(self, observations):
        """
        Performs training (called at end of epoch)

        Parameters
        ----------
        observations : list
            List of collected observations for multiple environments.

        Returns
        -------
        StatsAgent
            Agent train stats
        """

        # add observations to memory
        num_obs = self._store_observations(observations)

        # train multiple
        stats = []
        for _ in range(num_obs):
            stats.append(self._train())

        return stats


    def _store_observations(self, observations):
        """
        Stores list of observations in experience replay buffer

        Parameters
        ----------
        observations : list
            List of collected observations for multiple environments.

        Returns
        -------
        int
            Number of added experiences
        """

        num_added = 0

        # add observations to memory
        for name, obs_mult in observations:
            for obs_orig in obs_mult:
                obs = obs_orig.copy()

                # add dummy values for terminal state
                if not 'action' in obs:
                    obs['action'] = self.task.default_action
                if not 'reward' in obs:
                    obs['reward'] = 0
                if not 'next_state' in obs:
                    obs['next_state'] = self.task.default_rec_state()

                # apply state scaling
                obs['state'] = self.task.get_scaled_flattened_rec_state(obs['state'])
                obs['next_state'] = self.task.get_scaled_flattened_rec_state(obs['next_state'])

                # store
                # (if regret is activated, the observations are stored in an
                # episode buffer first, because the total number of steps is needed
                # to be stored in each individual experience)
                if self.par['agent_q']['regret']:
                    self.memory_episode.add(name, obs)

                    # move to real memory and clear episode buffer if terminal state
                    # is detected
                    if obs['terminal']:
                        num_steps = self.memory_episode.size1(name) - 1
                        for obs_eps in self.memory_episode.get1(name):
                            obs_eps['num_steps'] = num_steps
                            self.memory.add(obs_eps['name'], obs_eps)
                        self.memory_episode.reset1(name)
                else:
                    self.memory.add(name, obs)

                num_added += 1

        return num_added


    def _train(self):
        """
        Actual Implementation of Training Step

        Returns
        -------
        StatsAgent
            Agent train stats
        """

        # pylint: disable=too-many-locals

        # init agent train stats
        stats_agent = StatsAgent()

        # get parameters
        nn_double_q = self.par['agent_q']['nn_double_q']
        nn_target_q = self.par['agent_q']['nn_target_q']
        # target_update_freq = self.par['agent_q']['target_update_freq_train']
        batch_size = self.par['agent_q']['batch_size']
        discount_rate = self.par['agent_q']['discount_rate']
        learning_rate = self.par['agent_q']['learning_rate']
        regret = self.par['agent_q']['regret']

        # create mini batch
        batch = self.memory.sample(batch_size)
        states = np.array([each['state'] for each in batch])
        actions = np.array([each['action'] for each in batch])
        rewards = np.array([each['reward'] for each in batch])
        next_states = np.array([each['next_state'] for each in batch])
        terminals = np.array([each['terminal'] for each in batch])

        # adapt rewards to regret
        if regret:
            names = np.array([each['name'] for each in batch])
            num_steps = np.array([each['num_steps'] for each in batch])
            for i in range(batch_size):
                rewards[i] -= self.ref_rewards[names[i]] / max(1, num_steps[i])

        # get next Q values
        if nn_target_q and nn_double_q:
            feed_dict = {self.net['inputs']: next_states}
            target_q = self.sess.run(self.net['output'], feed_dict=feed_dict)

            feed_dict = {self.net['target_inputs']: next_states}
            target_target_q = self.sess.run(self.net['target_output'],
                                            feed_dict=feed_dict)

            target_actions = np.argmax(target_q, axis=1)
            q_next = np.zeros(batch_size)
            for i in range(batch_size):
                q_next[i] = target_target_q[i, target_actions[i]]

        elif nn_target_q:
            feed_dict = {self.net['target_inputs']: next_states}
            target_q = self.sess.run(self.net['target_output'], feed_dict=feed_dict)
            q_next = np.max(target_q, axis=1)

        else:
            feed_dict = {self.net['inputs']: next_states}
            target_q = self.sess.run(self.net['output'], feed_dict=feed_dict)
            q_next = np.max(target_q, axis=1)

        # get targets
        q_next[terminals] = 0
        targets = rewards + discount_rate * q_next

        # train neural network
        feed_dict = {self.net['inputs']: states,
                     self.net['target_q']: targets,
                     self.net['action']: actions,
                     self.net['learning_rate']: learning_rate}
        self.sess.run([self.net['opt']], feed_dict=feed_dict)

        # update target network
        # if nn_target_q and self.num_train % target_update_freq == 0:
        #     self.sess.run(self.update_target_op)
        if nn_target_q and self.num_train % self.target_update_freq == 0:
            self.sess.run(self.update_target_op)

        # stop time for training step
        stats_agent.time_stop()

        # increase training counter
        self.num_train += 1

        return stats_agent


    def get_miniagent(self, thread_id):
        """
        Creates Mini Agent

        The mini agent has limited features, which allows to take actions based
        on the current learning status, but does not allow training.

        Parameters
        ----------
        thread_id : int
            ID of thread that requires a copy for evaluation

        Returns
        -------
        MiniAgentQ
            Mini Agent
        """

        return MiniAgentQ(self.task, self.net['graph'], self.server[thread_id].target)


    def _neural_network_init(self):
        """
        Builds and initializes neural network
        """

        self._neural_network_def('current')
        self._neural_network_def('target')

        # init
        self.sess.run(tf.global_variables_initializer())


    def _neural_network_ref(self):
        """
        Specifies references to neural network

        Since neural networks can be loaded from file in order to resume
        training the referenes are set by looking for unique tensor / operation
        names.
        """

        self.net['graph'] = self.sess.graph
        self.net['inputs'] = self.net['graph'].get_tensor_by_name("current/inputs:0")
        self.net['output'] = self.net['graph'].get_tensor_by_name("current/output:0")
        self.net['loss'] = self.net['graph'].get_tensor_by_name("current/loss:0")
        self.net['opt'] = self.net['graph'].get_operation_by_name("current/train")
        self.net['target_q'] = self.net['graph'].get_tensor_by_name("current/target:0")
        self.net['action'] = self.net['graph'].get_tensor_by_name("current/action:0")
        self.net['learning_rate'] = self.net['graph'].get_tensor_by_name("current/learning_rate:0")
        self.net['abserrors'] = self.net['graph'].get_tensor_by_name("current/abserrors:0")
        self.net['target_inputs'] = self.net['graph'].get_tensor_by_name("target/inputs:0")
        self.net['target_output'] = self.net['graph'].get_tensor_by_name("target/output:0")


    def _neural_network_def(self, scope):
        """
        Defines the TensorFlow neural network

        Parameter
        ---------
        scope : str
            TensorFlow scope for neural network
        """

        # pylint: disable=too-many-locals

        # get parameters
        nn_dueling_q = self.par['agent_q']['nn_dueling_q']
        nn_hidden_nodes = self.par['agent_q']['nn_hidden_nodes']

        with tf.variable_scope(scope):
            # parameters
            learning_rate = tf.placeholder(tf.float32, [], name='learning_rate')

            # input layer
            inputs = tf.placeholder(tf.float32, [None, self.task.dim_rec_state()],
                                    name='inputs')

            # one hot encode actions to choose q value
            actions = tf.placeholder(tf.int32, [None], name='action')
            one_hot_actions = tf.one_hot(actions, self.task.dim_actions)

            # target qs
            target_q = tf.placeholder(tf.float32, [None], name='target')

            if nn_dueling_q:
                # hidden layers for value
                hidden11 = tf.layers.dense(inputs, nn_hidden_nodes,
                                           activation=tf.nn.sigmoid,
                                           name="hidden11")
                hidden12 = tf.layers.dense(hidden11, nn_hidden_nodes,
                                           activation=tf.nn.sigmoid,
                                           name="hidden12")
                hidden13 = tf.layers.dense(hidden12, 1, activation=None,
                                           name="hidden13")

                # hidden layers for advantage function
                hidden21 = tf.layers.dense(inputs, nn_hidden_nodes,
                                           activation=tf.nn.sigmoid,
                                           name="hidden21")
                hidden22 = tf.layers.dense(hidden21, nn_hidden_nodes,
                                           activation=tf.nn.sigmoid,
                                           name="hidden22")
                hidden23 = tf.layers.dense(hidden22, self.task.dim_actions,
                                           activation=None,
                                           name="hidden23")

                # output layer
                mean = tf.reduce_mean(hidden23, axis=1, keepdims=True)
                output_layer = hidden13 + tf.subtract(hidden23, mean)
                output = tf.identity(output_layer, name="output")
            else:
                # hidden layers
                hidden1 = tf.layers.dense(inputs, nn_hidden_nodes,
                                          activation=tf.nn.sigmoid,
                                          name="hidden1")
                hidden2 = tf.layers.dense(hidden1, nn_hidden_nodes,
                                          activation=tf.nn.sigmoid,
                                          name="hidden2")
                hidden3 = tf.layers.dense(hidden2, nn_hidden_nodes,
                                          activation=tf.nn.sigmoid,
                                          name="hidden3")

                # output layer
                output_layer = tf.layers.dense(hidden3, self.task.dim_actions,
                                               activation=None,
                                               name="output_layer")
                output = tf.identity(output_layer, name="output")

            # q values
            q_val = tf.reduce_sum(tf.multiply(output, one_hot_actions), axis=1)

            # absolute errors
            _ = tf.identity(tf.abs(target_q - q_val), name="abserrors")

            # loss function
            loss = tf.reduce_mean(tf.square(target_q - q_val), name='loss')

            # optimizer
            tf.train.AdamOptimizer(learning_rate).minimize(loss, name='train')


    def _neural_network_update_target(self, tau):
        """
        Operation to update target network weights

        Parameters
        ----------
        tau : float
            Level of update (0: no update; 1: full update)

        Return
        ------
        tensorflow operations
            Operations to update target values
        """

        dqn_vars = self.net['graph'].get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, 'current')
        target_vars = self.net['graph'].get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, 'target')
        op_holder = []
        for dqn_var, target_var in zip(dqn_vars, target_vars):
            op_holder.append(target_var.assign(dqn_var + (1-tau) * target_var))
        return op_holder


    def current_best(self, mode):
        """
        Indicates that current learning status is best so far

        The learning status is measured in different modes (e.g. training,
        testing_training, testing_testing) to specify the set of environments
        the agent befors better than before.

        Parameters
        ----------
        mode : str
            Mode the best performance has been encountered in (e.g. training)
        """

        # get parameters
        agent_dir = self.par['dir']['agent']

        # store graph
        saver = tf.train.Saver()
        saver.save(self.sess, os.path.join(agent_dir, 'best_' + mode, 'graph'))


    def finalize_tensorflow(self):
        """
        Finalizes Tensorflow
        """

        self.net = {}
        self.server = None
        self.update_target_op = None

        # close session
        self.sess.close()
        self.sess = None

        tf.reset_default_graph()


    def finalize(self):
        """
        Prepares the agent for storage

        This method has to be called before the agent data will be stored. Since
        Pickle has trouble with TensorFlow data, TensorFlow will store the
        neural network itself. Furthermore, all references to the neural
        network, TensorFlow servers and sessions will be deleted.
        """

        # get parameters
        agent_dir = self.par['dir']['agent']

        # store neural network using tensorflow routines and delete references
        # because pickle has trouble
        saver = tf.train.Saver()
        saver.save(self.sess, os.path.join(agent_dir, 'graph'))

        self.finalize_tensorflow()


class MiniAgentQ(MiniAgent):
    """
    Mini Agent of a Deep Q-Learning Agent

    The mini agent has limited features, which allows to take actions based on
    the current learning status, but does not allow training.

    Methods
    -------
    action(state)
        Selection of action (exploitation)
    action_rand(state)
        Selection of action (exploration)
    """

    def __init__(self, task, graph, target):
        """
        Parameters
        ----------
        task : Task
            Specified task to be learned (e.g., defines state and action spaces)
        graph : TensorFlow Graph
            Neural Network used for Q values
        target : TensorFlow Session Target
            Target specifies the server of cluster that is used for session
        """

        # init parent
        MiniAgent.__init__(self, task)

        # get neural network references
        self.net = {'graph' : graph,
                    'inputs' : graph.get_tensor_by_name("current/inputs:0"),
                    'output' : graph.get_tensor_by_name("current/output:0")}

        # start session
        self.sess = tf.Session(target=target, graph=graph)


    def action(self, state):
        """
        Selection of action (exploitation)

        Parameters
        ----------
        state : array_like
            State the action is based on

        Returns
        -------
        int
            Index of action
        """

        state_in = np.array(self.task.get_scaled_flattened_rec_state(state))
        feed_dict = {self.net['inputs']: state_in.reshape((1, *state_in.shape))}
        q_values = self.sess.run(self.net['output'], feed_dict=feed_dict)
        return np.argmax(q_values[0])


    def __del__(self):
        # close session
        self.sess.close()
