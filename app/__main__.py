#!/usr/bin/env python3
"""
Reinforcement Learning for Nonlinear Programming

In nonlinear programming solvers like nonlinear interior-point methods, the
practical performance is dependent on various parameter choices. Apart from
static parameters that can usually be adjusted by the user in a config file,
the methods also highly depend on parameters that have to be modified during
the solutoin process (e.g. the barrier parameter in interior-point methods). For
these, sophisticated update strategies have been developed that guarantee
convergence. In this project, the aim is to learn such adaptive parameter
updates of nonlinear programming solvers by using reinforcement learning
techniques.

This is the main module calling the training process. This includes
initialization or warmstart a old training instance as well as storing results.

Methods
-------
init_parameters()
    Initialization of parameters
init_process(par)
    Initialization of a fresh run
resume_process(par)
    Continuation of an old run
learning(learn, par)
    Learning process
finalize_process(task, agent, markov, learn, par)
    Finalize process and store results
"""

import sys
import os
import datetime
import pickle
from shutil import copyfile
import subprocess
import yaml

from app.markov import Markov
from app.learning import Learning
from app.agent_static import AgentStatic
from app.agent_q import AgentQ
from app.task import TaskBarrier


def init_parameters(counter, name, config):
    """
    Initialization of parameters

    Parameters
    ----------
    counter : int
        Index of current run
    name : str
        Name of run
    config : str
        Path to config file
    """

    # load options
    par = yaml.load(open(config, 'r'))

    # extend options with run naming#
    par['run'] = {}
    par['run']['name'] = name
    par['run']['config'] = config
    par['run']['resumes'] = 0
    par['dir'] = {}
    par['dir']['base'] = os.path.join('res', name)
    par['dir']['plots'] = par['dir']['base']
    par['dir']['config'] = os.path.join(par['dir']['base'], 'config')
    par['dir']['agent'] = os.path.join(par['dir']['base'], 'agent')
    par['dir']['obs'] = os.path.join(par['dir']['base'], 'obs')

    # adapt ports to current run
    par['threads']['ports1'] += 10*counter
    par['threads']['ports2'] += 10*counter

    return par


def init_process(par):
    """
    Initialization of a fresh run

    Parameters
    ----------
    par : dict
        Parameters
    """

    # create directories
    for _, directory in par['dir'].items():
        if not os.path.exists(directory):
            os.makedirs(directory)

    # copy parameter file
    filename_config = os.path.join(par['dir']['config'], 'config_' +
                                   str(par['run']['resumes']) + '.yml')
    filename_worhp = os.path.join(par['dir']['config'], 'worhp_' +
                                  str(par['run']['resumes']) + '.xml')
    copyfile(par['run']['config'], filename_config)
    copyfile('ext/worhp/worhp.xml', filename_worhp)

    # store git hash
    git_hash = subprocess.check_output(['git', 'rev-parse', 'HEAD']).decode()
    git_hash.replace('\n', '')
    with open(os.path.join(par['dir']['config'], 'git_commit.txt'), 'a') as file:
        file.write("{0:d};{1:s}\n".format(par['run']['resumes'], git_hash))

    # init task
    task = TaskBarrier(par)

    # init agent
    if par['learning']['agent'] == 'static':
        agent = AgentStatic(task, par)
    elif par['learning']['agent'] == 'q':
        agent = AgentQ(task, par)

    # init markov decision process
    markov = Markov(agent, par)

    # init learning
    learn = Learning(markov, agent, par)

    return task, agent, markov, learn


def resume_process(par):
    """
    Continuation of an old run

    Parameters
    ----------
    par : dict
        Parameters
    """

    with open(os.path.join(par['dir']['base'], 'workspace.file'), "rb") as file:
        num_resume = pickle.load(file)
        task = pickle.load(file)
        agent = pickle.load(file)
        learn = pickle.load(file)

    # restart agent
    agent.restart(par)

    # init markov decision process
    markov = Markov(agent, par)

    # restart learning
    learn.restart(markov)

    # update number of resume counter
    par['run']['resumes'] = num_resume + 1

    return task, agent, markov, learn


def finalize_process(task, agent, markov, learn, par):
    """
    Finalize process and store results

    Parameters
    ----------
    task : Task
        Task to be learned
    agent : Agent
        Agent used to learn the task
    markov : Markov
        Implementation of the Markov decision process
    learn : Learning
        Learning object
    par : dict
        Parameters
    """

    # store workspace
    markov.finalize()
    learn.finalize()
    agent.finalize()
    with open(os.path.join(par['dir']['base'], 'workspace.file'), "wb") as file:
        pickle.dump(par['run']['resumes'], file, pickle.HIGHEST_PROTOCOL)
        pickle.dump(task, file, pickle.HIGHEST_PROTOCOL)
        pickle.dump(agent, file, pickle.HIGHEST_PROTOCOL)
        pickle.dump(learn, file, pickle.HIGHEST_PROTOCOL)


def run(counter, name, config):
    """
    Process Caller

    Parameters
    ----------
    counter : int
        Index of current run
    name : str
        Name of run
    config : str
        Path to config file
    """

    # init parameters
    par = init_parameters(counter, name, config)

    # init / resume process
    if os.path.exists(os.path.join(par['dir']['base'], 'workspace.file')):
        task, agent, markov, learn = resume_process(par)
    else:
        task, agent, markov, learn = init_process(par)

    # learn
    learn.run()

    # finalize process
    finalize_process(task, agent, markov, learn, par)


if __name__ == '__main__':
    # read parameters
    NAMES = []
    CONFIGS = []
    CONFIG_PATH = ''
    for i, arg_val in enumerate(sys.argv):
        if arg_val == '--name':
            NAMES = sys.argv[i+1].split(',')
        elif arg_val == '--config':
            CONFIGS = sys.argv[i+1].split(',')
        elif arg_val == '--config_path':
            CONFIG_PATH = sys.argv[i+1]

    # default parameters
    if not NAMES:
        NOW = datetime.datetime.now()
        NAMES.append("{0:04d}_{1:02d}_{2:02d}_{3:02d}_{4:02d}_{5:02d}".format(
            NOW.year, NOW.month, NOW.day, NOW.hour, NOW.minute,
            NOW.second))
    if not CONFIGS:
        for _ in enumerate(NAMES):
            CONFIGS.append('config_std.yml')
    if not CONFIG_PATH:
        CONFIG_PATH = os.path.abspath('data/config')

    for i, (NAME, CONFIG) in enumerate(zip(NAMES, CONFIGS)):
        run(i, NAME, os.path.join(CONFIG_PATH, CONFIG))
