#!/usr/bin/env python3
"""
Collection of Outputs

Methods
-------
print_header(runname, num_resume, agent, problems, policy, episodes)
    Prints Program Header
print_epoch_header(mode, epoch)
    Prints Epoch Header
print_env_result(thread_id, env_name, reward, reward_best, t, success)
    Prints Environment Result
print_epoch_statistic_train(t, perf, perf_best)
    Prints Epoch Statistics (training)
print_epoch_statistic_test(t, perf, perf_best)
    Prints Epoch Statistics (testing)
"""

WIDTH = 61
BCOLORS = {
    'header' : '\033[95m',
    'ok_blue' : '\033[94m',
    'ok_green' : '\033[92m',
    'warning' : '\033[93m',
    'fail' : '\033[91m',
    'endc' : '\033[0m',
    'bold' : '\033[1m',
    'underline' : '\033[4m'}


def print_header(par):
    """
    Prints Program Header

    Parameters
    ----------
    par : dict
        Parameters
    """

    # get parameter
    resumes = par['run']['resumes']
    runname = par['run']['name']
    problems = par['learning']['problems']
    agent = par['learning']['agent']
    epochs = par['training']['epochs']
    policy = par['learning']['policy']

    print('')
    print('-'*WIDTH)
    print('      REINFORCEMENT LEARNING FOR NONLINEAR PROGRAMMING      ')
    print('-'*WIDTH)
    print('{0:10} {1:20} {2:10} {3:20}'
          .format("Run:", runname, "NLPs:", problems))
    print('{0:10} {1:20} {2:10} {3:20}'
          .format("Resumes:", str(resumes), "Agent:", agent))
    print('{0:10} {1:20} {2:10} {3:20}'
          .format("Episodes:", str(epochs), "Policy:", policy))
    print('')


def print_epoch_header(mode, epoch):
    """
    Prints Epoch Header

    Parameters
    ----------
    mode : str
        Mode of epoch (e.g. training)
    epoch : int
        Iteration count of current epoch
    """

    # get title of epoch
    if mode == 'training':
        title = str(epoch)
    else:
        title = mode.upper()

    print(" "*(WIDTH-len(title)) + title)
    print("-"*WIDTH)
    print(" {0:3} {1:18} {2:12} {3:10} {4:8} {5:4}"
          .format("PID", "PROBLEM", "REWARD", "BEST", "TIME", "EXIT"))
    print("-"*WIDTH)


def print_episode_statistic(stats_eps):
    """
    Prints Environment Result

    Parameters
    ----------
    stats_eps : StatsEpisode
        Environment run statistics
    """

    out = "({0:2}) {1:15}".format(stats_eps.thread_id, stats_eps.name)
    impr = stats_eps.reward / stats_eps.reward_best
    if impr <= 1:
        out += BCOLORS['ok_green']
    else:
        out += BCOLORS['fail']
    out += " {0:.4e}".format(stats_eps.reward) + BCOLORS['endc']
    if abs(stats_eps.reward_best) < float("inf"):
        out += " {0:.4e}".format(stats_eps.reward_best)
    else:
        out += " "*12
    out += " {0:.4e} ".format(stats_eps.time)
    if stats_eps.success:
        out += BCOLORS['ok_green']  + BCOLORS['bold'] + "   OK" + BCOLORS['endc']
    else:
        out += BCOLORS['fail'] + BCOLORS['bold'] + " FAIL" + BCOLORS['endc']
    print(out)


def print_epoch_statistic_train(stats_epo):
    """
    Prints Epoch Statistics (training)

    Parameters
    ----------
    stats_epo : StatsEpoch
        Epoch statistics
    """

    print("-"*WIDTH)
    print("{0:12} {1: .4e}".format("Time", stats_epo.time))
    out = "{0:12} ".format("Performance")
    if stats_epo.performance >= stats_epo.performance_best:
        out += BCOLORS['ok_green'] + "{0:+.4e}".format(stats_epo.performance)
    else:
        out += BCOLORS['fail'] + "{0:+.4e}".format(stats_epo.performance)
    out += BCOLORS['endc'] + " ("
    if stats_epo.performance_best >= 0:
        out += BCOLORS['ok_green'] + "{0:+.4e}".format(stats_epo.performance_best)
    else:
        out += BCOLORS['fail'] + "{0:+.4e}".format(stats_epo.performance_best)
    out += BCOLORS['endc'] + ")"
    print(out)
    print("{0:12} {1: .4e}".format("Exploration", stats_epo.explore_p))
    print("")


def print_epoch_statistic_test(stats_epo):
    """
    Prints Epoch Statistics (testing)

    Parameters
    ----------
    stats_epo : StatsEpoch
        Epoch statistics
    """

    print("-"*WIDTH)
    print("{0:12} {1: .4e}".format("Time", stats_epo.time))
    out = "{0:12} ".format("Performance")
    if stats_epo.performance >= stats_epo.performance_best:
        out += BCOLORS['ok_green'] + "{0:+.4e}".format(stats_epo.performance)
    else:
        out += BCOLORS['fail'] + "{0:+.4e}".format(stats_epo.performance)
    out += BCOLORS['endc'] + " ("
    if stats_epo.performance_best >= 0:
        out += BCOLORS['ok_green'] + "{0:+.4e}".format(stats_epo.performance_best)
    else:
        out += BCOLORS['fail'] + "{0:+.4e}".format(stats_epo.performance_best)
    out += BCOLORS['endc'] + ")"
    print(out)
    print("")


def print_learning_statistics(mode, stats_learn):
    """
    Prints Learning Statistics

    Parameters
    ----------
    mode : str
        Mode of epoch (e.g. training)
    stats_learn : StatsLearn
        Learning statistics
    """

    print("-"*WIDTH)
    print("STATISTICS " + mode.upper())
    print("-"*WIDTH)
    print("{0:12} {1: .4e}".format("Time", stats_learn.time))

    idx, best = stats_learn.get_performance_best()
    print("{0:12} {1: .4e} ({2:d})".format("Best", best, idx))
    print("")

    print("{0:16} {1:10} {2:11} {3:11}".format("", "MEAN", "STDDEV", "MEDIAN"))
    for tail in [0.01, 0.1, 0.25, 0.5, 1]:
        mean, std, median = stats_learn.get_performance_mean_std(tail)
        print("{0:.2f}-{1:7} {2: .4e} {3: .4e} {4: .4e}"
              .format(tail, "tail", mean, std, median))

    print("-"*WIDTH)
    print("")
