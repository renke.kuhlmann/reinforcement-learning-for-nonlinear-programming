#!/usr/bin/env python3
"""
Performance Module

Measures and tracks performance of training
"""

import os
import csv
import numpy as np
import matplotlib.pyplot as plt


class Performance:
    """
    Measures and tracks performance of training

    Since multiple environments are solved simultaneously, the reward of all
    environments has to be aggregated to one performance measure. This class
    therefore provides different performance measures (e.g. shifted geometric
    mean, performance profiles). The performance is compared to a given
    baseline. It is assumed that positive performance measure indicate superior
    performance compared to the baseline while negative performance measures
    indicate inferior performance.

    Methods
    -------
    geo_mean(vec, shift)
        Calculates shifted geometric mean
    performance_profile(mat, maxval, maxtau)
        Calculates performance profile by Dolan-More
    smooth(x, y, box_pts)
        Smoothes a given curve
    plot_performance(dir, name, epochs_perfs)
        Creates a plot of progress of performance measure over epochs
    plot_performance_profile(dir, name, mat_rewards, maxval)
        Creates a performance profile based on rewards
    baseline_reset()
        Resets the baseline
    get_baseline_rewards(names)
        Returns rewards of baseline
    reward_to_matrix(rewards)
        Creates matrix of rewards (incl baseline)
    performance(epoch, rewards)
        Calculates current performance measure
    performance_improve(new_perf, old_perf)
        Compares two performances
    """

    def __init__(self, par):
        """
        Parameters
        ----------
        par_nlp : dict
            Parameters
        """

        # parameter
        self.par = par

        # baseline for direct comparison
        self.baseline_rewards = []
        self.baseline_idx = {}

        # init baseline and rewards
        self.baseline_reset()


    @staticmethod
    def geo_mean(vec, shift):
        """
        Calculates shifted geometric mean

        Parameters
        ----------
        vec : array_like
            Rewards of all environments
        shift : float
            Positive shift in geometric mean

        Returns
        -------
        float
            Shifted geometric mean
        """

        return np.exp(np.sum(np.log(vec + shift)) / float(len(vec))) - shift


    @staticmethod
    def performance_profile(mat, maxval, maxtau=3.0):
        """
        Calculates performance profile by Dolan-More

        Instead of passing an indicator for success, it is assumed that all
        values below a certain threshold are successful and failure otherwise.

        Parameters
        ----------
        mat : array_like
            2D Matrix of rewards for all environments (1. dim: environments,
            2. dim: different runs)
        maxval : float
            Threshold value for success
        maxtau : float
            Maximum tau considered for creation

        Returns
        -------
        tau : array_like
            x-axis of performance profile
        rho : array_like
            y-axis of performance profile
        """

        # copy data
        data = np.copy(mat).astype(float)

        # set value to inf for unsuccessful runs
        for i in range(data.shape[0]):
            for j in range(data.shape[1]):
                if data[i, j] > maxval:
                    data[i, j] = float("inf")

        # calculate ratio
        taus = np.arange(0, maxtau, 0.01)
        ratio = np.zeros((data.shape[0], data.shape[1]))
        for j in range(data.shape[1]):
            for i in range(data.shape[0]):
                if data[i, j] == float("inf"):
                    ratio[i, j] = float("inf")
                else:
                    best_run = np.min(data[:, j], 0)
                    # pylint: disable=assignment-from-no-return
                    ratio[i, j] = np.divide(data[i, j], best_run)

        # calculate performance measure
        rhos = np.zeros((len(taus), data.shape[0]))
        for i in range(data.shape[0]):
            for j, tau in enumerate(taus):
                # pylint: disable=comparison-with-callable
                rhos[j, i] = np.sum(ratio[i, :] <= np.power(2, tau))
        rhos *= 100 / float(data.shape[1])

        return taus, rhos


    @staticmethod
    def smooth(x_val, y_val, rel_box_pts):
        """
        Smoothes a given function

        Parameters
        ----------
        x_val : array_like
            x values of function
        y_val : array_like
            y values of function
        box_pts : int
            Relative number of values (not range in x) to be considered for smoothing

        Returns
        -------
        x_smooth : array_like
            x values of smoothed function
        y_smooth : array_like
            y values of smoothed function
        """

        box_pts = int(np.ceil(rel_box_pts * len(y_val)))
        box = np.ones(box_pts) / box_pts
        y_smooth = np.convolve(y_val, box, mode='same')[box_pts//2:-box_pts//2]
        x_smooth = x_val[box_pts//2:-box_pts//2]
        x_smooth = np.concatenate(([x_val[0]], x_smooth, [x_val[-1]]), 0)
        y_smooth = np.concatenate(([y_val[0]], y_smooth, [y_val[-1]]), 0)
        return x_smooth, y_smooth


    @staticmethod
    def plot_performance(store_dir, name, epochs_perfs):
        """
        Creates a plot of progress of performance measure over epochs

        Parameters
        ----------
        store_dir : str
            Directory where to store plot
        name : str
            Name of plot
        epochs_perfs : list
            List of tuples (epochs, perfs, label), with arrays epochs (counter)
            and perfs (performance measures) and plot label
        """

        # create figure
        fig = plt.figure()
        axis = plt.gca()

        if not isinstance(epochs_perfs, list):
            epochs_perfs = [epochs_perfs]

        # plot performance measure
        maxx = 0
        for (epoch, perf, label) in epochs_perfs:
            # get color
            # pylint: disable=protected-access
            color = next(axis._get_lines.prop_cycler)['color']

            # plot all performance values
            plt.plot(epoch, perf, linewidth=1.0, alpha=0.3, color=color,
                     label='_nolegend_')

            # plot smoothed version
            epoch_smooth, perf_smooth = Performance.smooth(epoch, perf, 0.2)
            plt.plot(epoch_smooth, perf_smooth, linewidth=1.0, alpha=1,
                     color=color, label=label)

            # get max x value
            # pylint: disable=assignment-from-no-return
            maxx = np.maximum(maxx, epoch[-1])

        # plot baseline
        plt.plot([1, maxx], [0, 0], linewidth=1.0, color='black', label='baseline')

        # labels
        plt.xlabel("episodes")
        plt.ylabel("performance measure")
        plt.legend()

        # store
        if not os.path.exists(store_dir):
            os.makedirs(store_dir)
        plt.savefig(os.path.join(store_dir, name + '.png'), format='png')
        plt.close(fig)


    @staticmethod
    def plot_performance_profile(store_dir, name, mat_rewards, maxval):
        """
        Creates a performance profile based on rewards

        Parameters
        ----------
        store_dir : str
            Directory where to store plot
        name : str
            Name of plot
        mat_rewards : array_like
            2D Matrix of rewards for all environments (1. dim: environments,
            2. dim: different runs)
        maxval : float
            Threshold value for success
        """

        # create figure
        fig = plt.figure()

        # performance profile
        tau, rho = Performance.performance_profile(mat_rewards, maxval)

        # plot profile
        for i in range(rho.shape[1]):
            plt.plot(tau, rho[:, i], linewidth=1.0)

        # labels
        plt.xlabel("not more than 2^x-times worse than best solver")
        plt.ylabel("% of problems")
        legend = []
        legend.append('run')
        legend.append('baseline')
        plt.legend(legend)

        # settings
        plt.axis([0, 3, 0, 105])

        # store
        if not os.path.exists(store_dir):
            os.makedirs(store_dir)
        plt.savefig(os.path.join(store_dir, name + '.png'), format='png')
        plt.close(fig)


    def baseline_reset(self):
        """
        Resets the baseline
        """

        # get parameters
        baseline_name = self.par['performance']['baseline']

        self.baseline_rewards = []
        self.baseline_idx = {}
        file_name = os.path.join('.', 'data/baselines/' + baseline_name + '.csv')
        with open(file_name, 'rt') as file:
            reader = csv.DictReader(file, delimiter=';')
            i = 0
            for row in reader:
                self.baseline_rewards.append(float(row['REWARD']))
                self.baseline_idx[row['NAME']] = i
                i += 1


    def get_baseline_rewards(self, names):
        """
        Returns rewards of baseline

        Parameters
        ----------
        names : list
            List of environment names

        Returns
        -------
        dict
            Dictionary with key: name of environment and value: reference reward
        """

        rewards = {}
        for name in names:
            rewards[name] = self.baseline_rewards[self.baseline_idx[name]]
        return rewards


    def reward_to_matrix(self, rewards):
        """
        Creates matrix of rewards (incl baseline)

        Parameters
        ----------
        rewards : dict
            Dictionary mapping environment names to rewards

        Returns
        -------
        array_like
            2D Matrix of rewards for all environments (1. dim: environments,
            2. dim: different runs)
        """

        mat_rewards = np.zeros((2, len(rewards)))
        for i, (name, reward) in enumerate(rewards.items()):
            mat_rewards[0, i] = -reward
            mat_rewards[1, i] = -self.baseline_rewards[self.baseline_idx[name]]
        return mat_rewards


    def performance(self, rewards):
        """
        Calculates current performance measure

        Parameters
        ----------
        rewards : dict
            Dictionary mapping environment names to rewards

        Returns
        -------
        perf : float
            Performance measure of current epoch
        """

        # get parameters
        method = self.par['performance']['method']
        geoshift = self.par['performance']['geomean_shift']
        reward_maxval = self.par['nlp']['maxiter']

        # init
        perf = 0
        mat_rewards = self.reward_to_matrix(rewards)

        # calculate performance profiles
        if method == 'pprofiles':
            _, perf = self.performance_profile(mat_rewards, reward_maxval)
            perf = (sum(perf[:, 0]) - sum(perf[:, 1])) / perf.shape[0] / 100

        # calculate shifted geometric mean
        elif method == 'geomean':
            perf = -(self.geo_mean(mat_rewards[0, :], geoshift) /
                     self.geo_mean(mat_rewards[1, :], geoshift)) + 1

        return perf


    @staticmethod
    def performance_improve(new_perf, old_perf):
        """
        Compares two performances

        Parameters
        ----------
        new_perf : float
            New performance measure
        old_perf : float
            Old performance measure

        Returns
        -------
        bool
            True if new performance is better, False otherwise
        """

        return new_perf > old_perf
