#!/usr/bin/env python3
"""
Learning Module
"""

import os
import numpy as np

from app.performance import Performance
from app.stats import StatsLearn
import app.output as output


class Learning:
    """
    Learning Framework

    Calls Markov decision process in different learning modes training and testing
    where testing can be performed on the training programs or the testing programs.
    Thus, testing mode actually means, to run the environment with an agent without
    using random action selection (just exploitation). Statistics are generated
    and performance plots stored.

    Methods
    -------
    restart(markov)
        Restarts class after training resumes
    run()
        Run learning
    create_train_test_lists()
        Separates the environments in training and testing
    pretrain()
        Pretraining mode
    train()
        Training mdoe
    test_intermediate_train()
        Testing mode (training environments)
    test_intermediate_test()
        Testing mode (testing environments)
    test_final()
        Testing mode (all environments)
    store_intermediate_plots()
        Creates and stores performance plots
    finalize()
        Prepares learning for storage
    """

    # pylint: disable=too-many-instance-attributes

    def __init__(self, markov, agent, par):
        """
        Parameters
        ----------
        markov : Markov
            Markov decision process
        agent : Agent
            Agent to be trained
        par : dict
            Parameters
        """

        # arguments
        self.markov = markov
        self.agent = agent
        self.par = par

        # create training and testing lists
        self.problems = []
        self.train_list = []
        self.test_list = []
        self.create_train_test_lists()

        # shared workspace
        self.epoch = 0

        # performance measure
        self.perf = Performance(par)
        self.agent.update_regret_ref(self.perf.get_baseline_rewards(self.problems))

        # training and testing statistics
        self.stats_train = StatsLearn(self.train_list)
        self.stats_testtrain = StatsLearn(self.train_list)
        self.stats_testtest = StatsLearn(self.test_list)
        self.stats_testall = StatsLearn(self.problems)


    def restart(self, markov):
        """
        Restarts class after training resumes
        """

        self.markov = markov


    def run(self):
        """
        Run learning
        """

        # output
        output.print_header(self.par)

        # pretraining
        while self.pretrain():
            continue

        # run epochs
        for epoch in range(self.par['training']['epochs']):
            self.train()

            # testing
            if (epoch + 1) % self.par['testing']['freq'] == 0:
                self.test_intermediate_train()
                self.test_intermediate_test()
                self.store_intermediate_plots()

                # output result
                output.print_learning_statistics('training', self.stats_train)
                output.print_learning_statistics('testing_training', self.stats_testtrain)
                output.print_learning_statistics('testing_testing', self.stats_testtest)

        # final testing
        # self.test_final()

        # output result
        output.print_learning_statistics('training', self.stats_train)
        output.print_learning_statistics('testing_training', self.stats_testtrain)
        output.print_learning_statistics('testing_testing', self.stats_testtest)


    def create_train_test_lists(self):
        """
        Separates the environments in training and testing
        """

        # get parameters
        probs = self.par['learning']['problems']
        num_train = self.par['training']['num_prob']
        num_test = self.par['testing']['num_prob']

        # load problem names
        with open('data/envs/' + probs + '.txt', 'r') as file:
            self.problems = list(filter(None, file.read().split("\n")))

        # select random problems
        # pylint: disable=unsubscriptable-object
        np.random.seed(1)
        idx = np.random.choice(np.arange(len(self.problems)), size=num_train +
                               num_test, replace=False)

        # assign problem to training or testing
        self.train_list = []
        self.test_list = []
        for i in range(num_train):
            self.train_list.append(self.problems[idx[i]])
        for i in range(num_test):
            self.test_list.append(self.problems[idx[i + num_train]])


    def pretrain(self):
        """
        Pretraining mode

        Agent selects which environments are called until agent signals end of
        pretraining. This can be used to fill experience repaly buffers for
        example.

        Returns
        -------
        bool
            False: End of pretraining
        """

        # get list for pretraining
        pretrain_list = self.agent.pretrain_envs(self.train_list)

        # run multiple markov environments (problems)
        if pretrain_list:
            obs, stats_epo, _ = self.markov.run(pretrain_list, 'pretraining',
                                                self.epoch, self.stats_train)

            # process observations
            rewards = self._observe(obs)

            # set performance
            stats_epo.performance = float("nan")
            stats_epo.performance_best = float("nan")

            # update agent
            self.agent.update_regret_ref(rewards)

            # output
            output.print_epoch_statistic_train(stats_epo)

            return True

        return False


    def train(self):
        """
        Training Mode

        Runs an epoch on the training list and calculates performance on
        training set. Agent is in training mode (may take random actions).
        """

        # start timer
        self.stats_train.time_start()

        # update counter
        self.epoch += 1

        # create training list for current epoch
        if self.par['training']['rand_prob']:
            train_list = np.random.choice(self.train_list, size=len(self.train_list),
                                          replace=True)
        else:
            train_list = self.train_list

        # run multiple markov environments (problems)
        obs, stats_epo, _ = self.markov.run(train_list, 'training',
                                            self.epoch, self.stats_train)

        # process observations
        rewards = self._observe(obs)

        # calculate performance
        perf = self.perf.performance(rewards)
        impr = self.perf.performance_improve(perf, self.stats_train.performance_best)

        # update learning statistics
        stats_epo.performance = perf
        stats_epo.performance_best = self.stats_train.performance_best
        self.stats_train.update_epoch(self.epoch, rewards, perf)

        # update agent
        self.agent.update_regret_ref(rewards)

        # communicate improvement to agent
        if impr:
            self.agent.current_best('training')

        # stop timer
        self.stats_train.time_stop()

        # output
        output.print_epoch_statistic_train(stats_epo)


    def test_intermediate_train(self):
        """
        Testing mode (training environments)

        Runs an epoch on the training list and calculates performance on
        training set. Agent is in testing mode (does not take random actions).
        """

        # start timer
        self.stats_testtrain.time_start()

        # run multiple markov environments (problems)
        obs, stats_epo, _ = self.markov.run(self.train_list, 'testing_training',
                                            self.epoch, self.stats_testtrain)

        # process observations
        rewards = self._observe(obs)

        # calculate performance
        perf = self.perf.performance(rewards)
        impr = self.perf.performance_improve(perf, self.stats_testtrain.performance_best)

        # update learning statistics
        stats_epo.performance = perf
        stats_epo.performance_best = self.stats_testtrain.performance_best
        self.stats_testtrain.update_epoch(self.epoch, rewards, perf)

        # communicate improvement to agent
        if impr:
            self.agent.current_best('testing_training')

        # output
        output.print_epoch_statistic_test(stats_epo)

        # stop timer
        self.stats_testtrain.time_stop()

        # store observations
        self._store_intermediate_observations(obs)


    def test_intermediate_test(self):
        """
        Testing mode (testing environments)

        Runs an epoch on the testing list and calculates performance on
        testing set. Agent is in testing mode (does not take random actions).
        """

        # start timer
        self.stats_testtest.time_start()

        # run multiple markov environments (problems)
        obs, stats_epo, _ = self.markov.run(self.test_list, 'testing_testing',
                                            self.epoch, self.stats_testtest)

        # process observations
        rewards = self._observe(obs)

        # calculate performance
        perf = self.perf.performance(rewards)
        impr = self.perf.performance_improve(perf, self.stats_testtest.performance_best)

        # update learning statistics
        stats_epo.performance = perf
        stats_epo.performance_best = self.stats_testtest.performance_best
        self.stats_testtest.update_epoch(self.epoch, rewards, perf)

        # communicate improvement to agent
        if impr:
            self.agent.current_best('testing_testing')

        # output
        output.print_epoch_statistic_test(stats_epo)

        # stop timer
        self.stats_testtest.time_stop()

        # store observations
        self._store_intermediate_observations(obs)


    def test_final(self):
        """
        Testing mode (all environments)

        Runs an epoch on all environments and calculates performance on
        that set. Agent is in testing mode (does not take random actions).
        """

        # start timer
        self.stats_testall.time_start()

        # run multiple markov environments (problems)
        obs, stats_epo, _ = self.markov.run(self.problems, 'testing',
                                            self.epoch, self.stats_testall)

        # process observations
        rewards = self._observe(obs)

        # calculate performance
        perf = self.perf.performance(rewards)

        # update learning statistics
        stats_epo.performance = perf
        stats_epo.performance_best = self.stats_testall.performance_best
        self.stats_testall.update_epoch(self.epoch, rewards, perf)

        # output
        output.print_epoch_statistic_test(stats_epo)

        # stop timer
        self.stats_testall.time_stop()

        # store observations
        self._store_intermediate_observations(obs)


    @staticmethod
    def _observe(observations):
        """
        Extracts information from observation list

        Parameters
        ----------
        observations : list
            List of observations

        Return
        ------
        dict
            Cumulative rewards of different environments (key: environment name)
        """

        rewards_cum = {}
        for env_name, env_obs in observations:
            rewards_cum[env_name] = 0
            for obs in env_obs:
                if 'reward' in obs:
                    rewards_cum[env_name] += obs['reward']
        return rewards_cum


    def store_intermediate_plots(self):
        """
        Creates and stores performance plots
        """

        # get parameters
        reward_maxval = self.par['nlp']['maxiter']
        plt_dir = self.par['dir']['plots']

        # performance progress
        Performance.plot_performance(plt_dir, 'performance_train',
                                     (self.stats_train.epochs,
                                      self.stats_train.performances,
                                      'train'))
        Performance.plot_performance(plt_dir, 'performance_testtrain',
                                     (self.stats_testtrain.epochs,
                                      self.stats_testtrain.performances,
                                      'testtrain'))
        Performance.plot_performance(plt_dir, 'performance_testtest',
                                     (self.stats_testtest.epochs,
                                      self.stats_testtest.performances,
                                      'testtest'))
        Performance.plot_performance(plt_dir, 'performance',
                                     [(self.stats_train.epochs,
                                       self.stats_train.performances, 'train'),
                                      (self.stats_testtrain.epochs,
                                       self.stats_testtrain.performances,
                                       'testtrain'),
                                      (self.stats_testtest.epochs,
                                       self.stats_testtest.performances,
                                       'testtest')])

        # performance profiles
        rewward_matrix = self.perf.reward_to_matrix(self.stats_train.rewards)
        Performance.plot_performance_profile(plt_dir, 'profile_train',
                                             rewward_matrix, reward_maxval)
        rewward_matrix = self.perf.reward_to_matrix(self.stats_testtrain.rewards)
        Performance.plot_performance_profile(plt_dir, 'profile_testtrain',
                                             rewward_matrix, reward_maxval)
        rewward_matrix = self.perf.reward_to_matrix(self.stats_testtest.rewards)
        Performance.plot_performance_profile(plt_dir, 'profile_testtest',
                                             rewward_matrix, reward_maxval)


    def _store_intermediate_observations(self, observations):
        """
        Stores observations to file

        Parameters
        ----------
        observations : list
            List of observations
        """

        for obs in observations:
            filename = os.path.join(self.par['dir']['obs'], obs[0])
            with open(filename, 'w') as file:
                file.write('STATE;ACTION;REWARD\n')
                for obs_single in obs[1]:
                    file.write('[')
                    for state in obs_single['state'][-1]:
                        file.write("{0:+.6e},".format(state))
                    file.write('];')
                    if 'action' in obs_single:
                        file.write("{0:+.6e}".format(obs_single['action']))
                    file.write(';')
                    if 'reward' in obs_single:
                        file.write("{0:+.6e}".format(obs_single['reward']))
                    file.write('\n')


    def finalize(self):
        """
        Prepares learning for storage
        """

        # markov class cannot be stored, so delete reference
        self.markov = None
