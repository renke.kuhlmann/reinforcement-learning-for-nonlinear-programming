
[![pipeline status](https://gitlab.com/renke.kuhlmann/reinforcement-learning-for-nonlinear-programming/badges/master/pipeline.svg)](https://gitlab.com/renke.kuhlmann/reinforcement-learning-for-nonlinear-programming/commits/master)
[![coverage report](https://gitlab.com/renke.kuhlmann/reinforcement-learning-for-nonlinear-programming/badges/master/coverage.svg)](https://gitlab.com/renke.kuhlmann/reinforcement-learning-for-nonlinear-programming/commits/master)


# Reinforcement Learning for Nonlinear Programming

In nonlinear programming solvers like nonlinear interior-point methods, the
practical performance is dependent on various parameter choices. Apart from
static parameters that can usually be adjusted by the user in a config file,
the methods also highly depend on parameters that have to be modified during
the solutoin process (e.g. the barrier parameter in interior-point methods). For
these, sophisticated update strategies have been developed in the literature.
In this project, the aim is to learn such adaptive parameter updates of nonlinear
programming solvers by using reinforcement learning techniques that lead to
superior practical performance of the nonlinear programming solver.


## Build

The application can deal with any external program that sends states, rewards and
receives actions using the socket protocol specified in `app/markov.py`. Here,
the default is the training of [WORHP](https://worhp.de/), because of its reverse
communication paradigm allowing to access internal parameter updates through the
regular user interface. If training of [WORHP](https://worhp.de/) shall be
performed on [CUTEst](https://github.com/ralna/CUTEst) problems, do:

1. Place CUTEst lib in `ext/cutest/lib`, CUTEst include files in `ext/cutest/include`
and CUTEst problem files (`AUTOMAT.d`, `ELFUN.f`, `EXTER.f`, `GROUP.f`, `OUTSDIF.d`, `RANGE.f`)
in `ext/cutest/sif/<problem-name>/OUTSDIF.d` etc.

2. Place WORHP lib in `ext/worhp/lib` and include files in `ext/worhp/include`

3. Run `./waf`


## Run

### Application

Run:
```bash
python -m app --name <name1>,<name2> --config <config1>,<config2> --config_path <path>
```
or simply (all parameters are optional):
```bash
python -m app
```
Parameters are
- `--name`: Names of training run (default: current timestep, single run)
- `--config`: Name of config files (default: `config_std.yml`)
- `--config_path`: Path to config files (default: `data/config`)

Results will be stored in `res/<name>`. If training is started with a run name
that already exists in `res`, training is resumed for that run.

### Unit and Integration Tests

Run:
```bash
python -m test --tests --inttests
```
Parameters are
- `--tests`: Enable unit tests
- `--inttests`: Enable integration tests


## Reference

If you find this useful for your work, please consider to cite the following
[paper](https://www.researchgate.net/publication/335259362_Learning_to_steer_nonlinear_interior-point_methods):

    @article{Kuhlmann2019,
        author = "Kuhlmann, Renke",
        title = "Learning to steer nonlinear interior-point methods",
        journal = "EURO Journal on Computational Optimization",
        year = "2019",
        doi = "10.1007/s13675-019-00118-4"
    }
