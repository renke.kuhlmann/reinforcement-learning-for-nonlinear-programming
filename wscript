#!/usr/bin/env python3

import errno
import os
import sys
from waflib import Build, Configure, Options
Configure.autoconfig = True

CUTEST_DIR = "ext/cutest"
CUTEST_INC_DIR = CUTEST_DIR + "/include"
CUTEST_LIB_DIR = CUTEST_DIR + "/lib"
CUTEST_PROBLEMS_DIR = CUTEST_DIR + "/sif"

WORHP_DIR = "ext/worhp"
WORHP_INC_DIR = WORHP_DIR + "/include"
WORHP_LIB_DIR = WORHP_DIR + "/lib"
WORHP_CONFIG_DIR = WORHP_DIR

def options(ctx):
    ctx.load('compiler_cxx compiler_c compiler_fc')

def configure(ctx):
    ctx.load('compiler_cxx compiler_c compiler_fc')

    ctx.env.append_value('DEFINES', ['NDEBUG'])
    ctx.env.append_value('CXXFLAGS', ['-O3', '-std=c++11', '-Wall', '-Wextra',
        '-Werror', '-Wzero-as-null-pointer-constant'])
    ctx.env.append_value('FCFLAGS', ['-O3', '-ffree-line-length-none'])

    ctx.check_cc(lib='gfortran', uselib_store='GFORTRAN', mandatory=True)
    ctx.check_cc(lib='m', uselib_store='LIBM', mandatory=True)
    ctx.check(lib='cutest', libpath=os.path.abspath(CUTEST_LIB_DIR),
        uselib_store='CUTEST', mandatory=True)
    ctx.check(lib='worhp', libpath=os.path.abspath(WORHP_LIB_DIR),
        uselib_store='WORHP', mandatory=True)

def build(ctx):
    for problem in get_cutest_problems(ctx):
        problem_dir = os.path.join(CUTEST_PROBLEMS_DIR, problem)
        outsdifd_path = os.path.abspath(os.path.join(problem_dir, 'OUTSDIF.d'))
        ctx.program(target=os.path.join('worhp_ai', problem),
            includes=[CUTEST_INC_DIR, WORHP_INC_DIR],
            defines=['THIS_PROBLEM_NAME="' + problem + '"',
                     'OUTSDIFD_FILENAME="' + outsdifd_path + '"',
                     'WORHP_CONFIG="' + os.path.join(WORHP_CONFIG_DIR,
                        "worhp.xml") + '"'],
            name=problem,
            source=ctx.path.ant_glob(os.path.join(WORHP_DIR, '*.c')) +
                ctx.path.ant_glob(os.path.join(problem_dir, '*.f')),
            use='WORHP CUTEST GFORTRAN LIBM',
            rpath=[os.path.abspath(WORHP_LIB_DIR)])

def get_cutest_problems(ctx):
    problems = set()
    if not ctx.targets:
        problems.update(os.listdir(CUTEST_PROBLEMS_DIR))
    else:
        targets = ctx.targets.split(',')
        for target in targets:
            if os.path.isfile(target):
                with open(target, 'r') as list_file:
                    problems_list = list_file.read().splitlines()
                    problems.update(problems_list)
                ctx.targets = ctx.targets.replace(target, ','.join(problems_list))
            else:
                problems.add(target)
    for problem in problems:
        if os.path.isdir(os.path.join(CUTEST_PROBLEMS_DIR, problem)):
            yield problem
